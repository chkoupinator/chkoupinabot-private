from discord.ext import commands
import discord
import os
import json
import copy
from lang.lang_wrap import LangWrap
from utils import persistence
from cogs import achievements
from discord.ext.commands.errors import BadArgument
import traceback
import sys
from utils.converters import MemberConverter
from chkoupinabot import ChkoupinaBot


lang_settings = {
    "cmds": {
        "profile": {
            "help": {
                "name": "profile",
                "usage": "`{prefix}profile [user]`\n"
                         "`[user]` can be a nickname, a username#disc, an @mention or a user id\n"
                         "shows the profile of the [user], or yours if no user is given",
                "short_desc": "the command used to show your profile or others' profiles",
                "long_desc": "a command to show customizable chkoupinabot profiles, it can be used for your own "
                             "profile or for someone else's, to set your profile you can use the `setprofile` command"
            },
            "bad_username": "error: user named `{name}` not found"
        },
        "setprofile": {
            "help": {
                "name": "setprofile",
                "usage": "`{prefix}setprofile <field> <value>`\n`<field>` has to be one of the following: \n- "
                         "`description` or `bio` to set your description.\n- `ign`, `epicname` or `gameid` to set "
                         "your in game name. \n__**`Save the World:`**__\n- `powerlevel` or `pl` to set your "
                         "powerlevel (number).\n- `main`, `mainhero` or `hero` to set your mainly played hero(es). "
                         "\n-`schematics` or `schems` to showcase some of the schematics (weapons/traps) you like the "
                         "most.\n__**`BattleRoyale:`**__\n- `wins` or `brwins` to set your wins (number).\n- `kd` or "
                         "`kd ratio` to show your kill/death ratio.\n- `games played` to show your games played ("
                         "number).\n- `bptier` or `battlepass` to set your current battle pass tier (number).\n- "
                         "`brlevel` or `seasonlevel` to set your current seasonal level(number).\n`<value>` has to be "
                         "either text (suppports line jumps) or an integer number when specified (no float, "
                         "no special characters).\nSets the value of the `<field>` to `<value>`",
                "short_desc": "the command used to modify your profile",
                "long_desc": "a command that can be used to set the settable fields of your chkoupinabot user profile, "
                             "you can use the `profile` command to show your profile"
            },
            "success": "field `{field}` successfully set! <:pepe_ok_hand:382198933134245888>",
            "invalid_field": "the field you are trying to set is not a valid field, to find the valid fields please do "
                             "`{prefix}help setprofile`",
            "not_a_number": "the field you selected can only take a number",
            "no_args": "error: this command needs a __single word__ (can have underscores sometimes) `field` and a "
                       "`value` separated with one space, please do `{prefix}help setprofile` for more help",
            "region": "use `{prefix}region` to set your region, or `{prefix}help region` for more info about how to "
                      "set your region",
            "platform": "use `{prefix}platform` to set/add/reset your platform(s), do `{prefix}help platform for more "
                        "info."
        },
        "region": {
            "invalid_region": "error: the region you selected is invalid, the regions supported by the game and this "
                              "discord are {valid_regions}",
            "regions_list": "the valid regions supported by the game and this discord are: {valid_regions}.\n"
                            "please do `{prefix}region <your region from the list here>` to select a region "
                            "(example:  `{prefix}region EU`)",
            "valid_regions": "`Asia`, `Brazil`, `EU`, `NA-E`, `NA-W` or `OCE`",
            "role_already_selected": "it seems like you already have the role of that region, your profile has been "
                                     "updated anyways",
            "success": "region successfully selected <:pepe_ok_hand:382198933134245888>",
            "help": {
                "name": "region",
                "usage": "`{prefix}region <region>`\n`<region>` can be `Asia`, `Brazil`, `EU`, `NA-E`, `NA-W` or `OCE`"
                         "\nsets your region to `<region>` changing it in your profile and giving you its role",
                "short_desc": "used to set your region",
                "long_desc": "allows you to pick up a region from one of the supported in game selectable regions"
            }
        },
        "platform": {
            "platforms_list": "the valid platforms supported by the game and this discord are: {valid_platforms}\n"
                              "please do `{prefix}help platform` for more info",
            "valid_platforms": "`PC`, `XB1`, `PS4`, `iOs` or `Switch`",
            "invalid_platform": "error: the platform you selected is invalid, please select a platform from "
                                "{valid_platforms}\nplease do `{prefix}help platform` for more info",
            "reset_success": "platforms successfully reset <:pepe_ok_hand:382198933134245888>",
            "set_success": "platform successfully set <:pepe_ok_hand:382198933134245888>",
            "role_already_selected": "it seems like you already have the role of that platform, your profile has been "
                                     "updated and the other roles have been removed successfully",
            "add_success": "platform successfully added <:pepe_ok_hand:382198933134245888>",
            "role_already_added": "it seems like you already have the role of that platform, your profile has been "
                                     "checked for that role",

            "help": {
                "name": "platform",
                "usage": "`{prefix}platform [add <platform> | set <platform> | reset | <platform>]`\n"
                         "`<platform>` can be one of the following `PC`, `XB1`, `PS4`, `iOs` or `Switch` and is only "
                         "required if you use `set` `add` or leave it empty which defaults to `add`\n"
                         "adds a platform if called with `add` or nothing before the `<platform>`\n"
                         "sets a platform if called with `set` or \n"
                         "resets the platforms and removes all the platforms and their roles if called with `reset`",
                "short_desc": "platform selection/management command",
                "long_desc": "command used to add, set or reset your platform(s), more info on the usage part"
            }
        }
    },
    "embed": {
        "title": "user profile for {user.name}",
        "game": {
            "cat_name": "fortnite info",
            "name": "epic games name",
            "region": "region",
            "platforms": "platform(s)",
        },
        "stw": {
            "cat_name": "save the world",
            "pl": "powerlevel",
            "main_heroes": "main heroe(s) played",
            "main_schematics": "mainly used schematics"
        },
        "trade": {
            "cat_name": "trade",
            "have": "have",
            "want": "want"
        },
        "br": {
            "cat_name": "battleroyale",
            "wins": "wins",
            "kd_ratio": "kill/death ratio",
            "games_played": "games played",
            "battlepass_tier": "seasonal battlepass tier",
            "level": "current season level"
        },
        "karma": {
            "cat_name": "karma",
            "given_today": "given today",
            "given_total": "total given",
            "questions": "questions",
            "trade": "trade",
            "lfg": "save the world",
            "br": "battleroyale",
            "total": "total received"
        },
        "infringements": {
            "cat_name": "rule infringements and actions taken against the user",
            "warnings": "warnings",
            "mutes": "mutes",
            "bans": "bans"
        },
        "mod": {
            "cat_name": "actions taken by the mod",
            "warnings": "warnings given",
            "mutes": "mutes issued",
            "bans": "hammers down"
        },
        "inventory": {
            "cat_name": "inventory",
            "notification_roles": "notification roles",
            "colour_roles": "colour roles",
            "other": "misc"
        },
        "achievements": "achievements",
        "exp_multiplier": "experience multiplier",
        "social": {
            "title": "social",
            "formatting": "**__current season {season}:__**\n"
                          "**level:** {profile_social[month_level]}\n"
                          "**exp:** {profile_social[level_exp]:.1f}/{needed_exp} "
                          "(total {profile_social[month_exp]:.1f})\n"
                          "**__exp boosts:__**\n"
                          "**daily exp boost:** {profile_social[daily_exp_multiplier]:.0%} "
                          "({profile_social[daily_boost_messages_left]} messages left)\n"
                          "**seasonal exp boost:** {profile_social[monthly_exp_multiplier]:.0%}\n"
                          "**__all time:__**\n"
                          "**level sums:** {profile_social[total_level]}\n"
                          "**total level:** {total_level}\n"
                          "**exp:** {profile_social[total_exp]:.0f}\n"
                          "**__currency:__** {profile_social[currency]} {currency}"
        },
        "activity_per_channel_today": "activity per channel today",
        "activity_per_channel_month": "activity per channel this month"


    },
    "help": {
        "name": "profiles module",
        "short_desc": "the module that contains the profile functionalities",
        "long_desc": "the module that handles showing and modifying user profiles"
    }
}


profile_structure = {
    "id": 0,
    "name": "",
    "avatar": "",
    "description": "you know nothing about me, brother\n~~mada mada~~",
    "colour": "",
    "link": "",
    "game": {
        "name": "",
        "region": "",  # Can be Asia, Brazil, EU, NA-E, NA-W, OCE
        "platforms": []  # Can be PC, XB1, PS4, iOs, Switch
    },
    "stw": {
        "pl": 0,
        "main_heroes": "",
        "main_schematics": ""
    },
    "trade": {
        "have": [],
        "want": []
    },
    "br": {
        "wins": 0,
        "kd_ratio": "",
        "games_played": "",
        "battlepass_tier": 0,
        "level": 0
    },
    "karma": {
        "given_today": 0,
        "given_total": 0,
        "questions": 0,
        "trade": 0,
        "lfg": 0,
        "br": 0,
        "total": 0
    },
    "infringements": {
        "warnings": [],
        "mutes": [],
        "bans": [],
    },
    "mod": {
        "warnings": [],
        "mutes": [],
        "bans": []
    },
    "inventory": {
        "colour_roles": [],
        "other": []  # Can be exp boosts, currency gift cards,
    },
    "shown_achievements": [],
    "achievements": [],
    "social": {
        "daily_exp_multiplier": 1,
        "daily_boost_messages_left": 0,
        "monthly_exp_multiplier": 1,
        "currency": 0,
        "total_exp": 0,
        "month_exp": 0,
        "month_exp_track": {},
        "level_exp": 0,
        "month_level": 0,
        "month_level_track": {},
        "total_level": 0,
        "activity_per_channel_today": {

        },
        "activity_per_channel_month": {

        },
        "activity_per_channel_month_track": {

        },
        "quality_messages_today": 0,
        "messages_today": 0,
        "quality_messages": 0,
        "messages": 0,
        "last_message": 0,
        "daily_available": 1,
        "daily_streak": 0,
        "exp_from_vc_today": 0,
        "max_voice_exp_hit_today": False
    },
    "tracking": {
        "total_currency": 0,
        "used_currency": 0,
        "received_currency_gifts": 0,
        "sent_currency_gifts": 0,
        "purchases_made": 0,
        "commands_used": 0,
        "time_in_voice": 0,
        "max_voice_exp_hit": 0,
        "exp_boosts_used": 0,
        "exp_boosts_gifted": 0,
        "exp_boosts_received": 0
    }
}

conformity_checked = []


def load(userid):
    if userid not in conformity_checked:
        with open(os.path.join("profiles", f"{userid}.json"), 'r') as profile_file:
            profile = json.load(profile_file)
            conformity_check(profile)
            save(profile)
            return profile
    else:
        with open(os.path.join("profiles", f"{userid}.json"), 'r') as profile_file:
            return json.load(profile_file)


def safe_load(user):
    try:
        return load(user.id)
    except FileNotFoundError:
        return create(user)


def save(profile):
    with open(os.path.join("profiles", f'{profile["id"]}.json'), 'w') as profile_file:
        json.dump(profile, profile_file, indent=2)
        profile_file.truncate()
    return profile


def reset(userid):
    pass
    # TODO add profile resets (not a priority)


def to_level_up(level):
    if level < 10:
        return 100
    elif level < 50:
        return 10 * level
    elif level < 100:
        return 15 * level
    else:
        return 20 * level


def exp_based_total_level(exp):
    level = 0
    while True:
        if exp > to_level_up(level):
            exp -= to_level_up(level)
            level += 1
        else:
            return level


def daily_reset(profile):
    profile["social"]["daily_exp_multiplier"] = 2
    profile["social"]["daily_boost_messages_left"] = 100
    profile["social"]["activity_per_channel_today"] = dict()
    profile["social"]["quality_messages_today"] = 0
    profile["social"]["messages_today"] = 0
    profile["social"]["daily_available"] += 1
    profile["social"]["max_voice_exp_hit_today"] = False
    profile["social"]["exp_from_vc_today"] = 0
    profile["karma"]["given_today"] = 0
    return profile


def jul_sept_fallback(profile, guild: discord.Guild):
    member = guild.get_member(profile["id"])  # type: discord.Member
    if member is not None:
        level = 0
        for role in member.roles:  # type: discord.Role
            if role.id == 474057854601658398:
                level = 100
            elif role.id == 474058050500689931:
                level = 75
            elif role.id == 474058045056614430:
                level = 50
            elif role.id == 474058288540024844:
                level = 25
            elif role.id == 474058290934972416:
                level = 10
            elif role.id == 474058562704900097:
                level = 5
            elif role.id == 474058286417575938:
                level = 1
            else:
                continue
            break
        if level:
            profile["social"]["month_level_track"]["Jul-Sep 2018"] = level


def oct_dec_achievements_mess(profile):
    for achievement in profile["achievements"]:
        if "oct-dec_2018" in achievement["name"]:
            if "first" in achievement["name"]:
                achievement["name"] = "oct-dec_2018"
            else:
                achievement["name"] = "oct-dec_2018_first"


def monthly_backup(profile, previous_season):
    for key in ["month_level", "month_exp", "activity_per_channel_month"]:
        profile["social"][f"{key}_track"][previous_season] = profile["social"][key]


def monthly_reset(profile):
    profile["social"]["monthly_exp_multiplier"] = 1
    profile["social"]["month_level"] = 0
    profile["social"]["month_exp"] = 0
    profile["social"]["level_exp"] = 0
    profile["social"]["activity_per_channel_month"] = dict()
    return profile


legacy_level_roles = {
        338576405749170176: ["Flinger Dark Purple", 30],
        338525339003125761: ["Blaster Purple", 27],
        334093674130571271: ["Poisonous Lobber Green", 25],
        334093657969786880: ["Beehive Light Orange", 23],
        334027237793857556: ["Troll Blue", 20],
        332221420945932289: ["Sploder Dark Red-Orange", 17],
        332221417443819520: ["Pitcher Light Blue", 15],
        332221417506471938: ["Smasher Light Purple", 13],
        332221418420830209: ["Lobber Pink", 10],
        334883925014872075: ["Husky Husk Browner", 7],
        332221419054301184: ["Husk Brown", 5],
        332221420010733568: ["Mini Husk Grey", 3],
    }

legacy_region_roles = {
    448811930392592385: "Asia",
    448811967436554262: "Brazil",
    334031174496288779: "EU",
    334031174957531138: "NA",
    460147701435072544: "NA-E",
    460147705369329664: "NA-W",
    334031175649591298: "OCE"
}
legacy_platform_roles = {
    334031177067397130: "PC",
    334031177578971136: "XB1",
    334031418445398026: "PS4",
    426065596006793216: "iOs"

}


def create(user):
    print(f"Creating profile for user `{user.name}` userid `{user.id}`")

    profile = copy.deepcopy(profile_structure)

    highest_level = 0
    bonus = 0
    if user.discriminator == "0000":
        profile["id"] = user.id
        profile["name"] = user.name
        return save(profile)

    for role in user.roles:
        if role.id in legacy_level_roles.keys():
            profile["inventory"]["colour_roles"].append(legacy_level_roles[role.id][0])
            level = legacy_level_roles[role.id][1]
            profile["social"]["currency"] += level * 150 + 150  # a normal user gets level * 50 + 50 every level up
            profile["tracking"]["total_currency"] += level * 150 + 150
            # but since the roles are spread every 2.5 levels on average it's better to do it this way
            if highest_level < level:
                highest_level = level
            bonus += 1
        elif role.id in legacy_region_roles.keys():
            profile["game"]["region"] = legacy_region_roles[role.id]
        elif role.id in legacy_platform_roles.keys():
            profile["game"]["platforms"].append(legacy_platform_roles[role.id])

    profile["social"]["total_level"] = highest_level + bonus
    if highest_level >= 7:
        profile["achievements"].append(achievements.unlocked("legacy_member", level=3))
    elif highest_level >= 3:
        profile["achievements"].append(achievements.unlocked("legacy_member", level=2))
    elif highest_level >= 1:
        profile["achievements"].append(achievements.unlocked("legacy_member", level=1))

    profile["id"] = user.id
    profile["name"] = user.name
    return save(profile)


def conformity_check(profile):
    for key in profile_structure.keys():
        try:
            if isinstance(profile[key], dict):
                for sub_key in profile_structure[key]:
                    try:
                        profile[key][sub_key]
                    except KeyError:
                        profile[key][sub_key] = copy.deepcopy(profile_structure[key][sub_key])
                        print(f"adding missing key {key}.{sub_key} in {profile['name']}'s profile, id {profile['id']}")
        except KeyError:
            profile[key] = copy.deepcopy(profile_structure[key])
            print(f"adding missing key {key} in {profile['name']}'s profile, id {profile['id']}")
    for key in list(profile.keys()):
        if key not in profile_structure.keys():
            print(f"deleting key {key} in {profile['name']}'s profile with the id {profile['id']} that had "
                  f"the value {profile[key]}")
            del profile[key]

        else:
            if isinstance(profile[key], dict):
                for sub_key in list(profile[key].keys()):
                    if sub_key not in profile_structure[key].keys():
                        print(f"deleting key {key}.{sub_key} in {profile['name']}'s profile with the id {profile['id']}"
                              f" that had the value {profile[key][sub_key]}")
                        del profile[key][sub_key]


class ProfilesCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        self.settable_text_fields = {
            "description": "description",
            "bio": "description",
            "ign": "game.name",
            "epicname": "game.name",
            "epic_name": "game.name",
            "gameid": "game.name",
            "game_id": "game.name",
            "main_hero": "stw.main_heroes",
            "mainhero": "stw.main_heroes",
            "hero": "stw.main_heroes",
            "main_heroes": "stw.main_heroes",
            "mainheroes": "stw.main_heroes",
            "heroes": "stw.main_heroes",
            "main_heros": "stw.main_heroes",
            "heros": "stw.main_heroes",
            "main": "stw.main_heroes",
            "schematics": "stw.main_schematics",
            "schematic": "stw.main_schematics",
            "schems": "stw.main_schematics",
            "schem": "stw.main_schematics",
            "kd": "br.kd_ratio",
            "kda": "br.kd_ratio",
            "kd_ratio": "br.kd_ratio",
        }
        self.settable_int_fields = {
            "powerlevel": "stw.pl",
            "pl": "stw.pl",
            "power_level": "stw.pl",
            "wins": "br.wins",
            "brwins": "br.wins",
            "br_wins": "br.wins",
            "bp_tier": "br.battlepass_tier",
            "bptier": "br.battlepass_tier",
            "bp": "br.battlepass_tier",
            "battlepass": "br.battlepass_tier",
            "br_level": "br.level",
            "brlevel": "br.level",
            "season_level": "br.level",
            "seasonlevel": "br.level",
            "games_played": "br.games_played",
            "gamesplayed": "br.games_played",
            "br_games": "br.games_played",
            "played": "br.games_played"
        }
        self.regions_roles = {
            "Asia": 448811930392592385,
            "Brazil": 448811967436554262,
            "EU": 334031174496288779,
            "NA-E": 460147701435072544,
            "NA-W": 460147705369329664,
            "NA": 334031174957531138,
            "OCE": 334031175649591298
        }
        self.platform_roles = {
            "PC": 334031177067397130,
            "XB1": 334031177578971136,
            "PS4": 334031418445398026,
            "iOs": 426065596006793216,
            "Android": 498197000496480276,
            "Switch": 460147698377555978
        }

    def embed_maker(self, profile, user, *, mode=None):
        embed = discord.Embed(title=self.lang("embed.title").format(user=user),
                              description=profile["description"], url=profile["link"],
                              colour=user.colour.value if isinstance(user, discord.Member) else 0x000)
        if profile["avatar"]:
            embed.set_thumbnail(url=profile["avatar"])
        else:
            embed.set_thumbnail(url=user.avatar_url)
        simple_dict_categories = ["game", "stw", "br", "karma"]
        for key in simple_dict_categories:
            if profile[key] != profile_structure[key]:
                m = str()
                for field in profile[key].keys():
                    if profile[key][field] == profile_structure[key][field]:
                        continue
                    m += self.lang(f"embed.{key}.{field}")
                    if isinstance(profile[key][field], list):
                        m += f": {', '.join(profile[key][field])}\n"
                    else:
                        m += f": {profile[key][field]}\n"
                embed.add_field(name=self.lang(f"embed.{key}.cat_name"), value=m, inline=True)
        if profile["shown_achievements"]:
            embed.add_field(name=self.lang("embed.achievements"),
                            value=self.bot.achievements.short_lister(profile["shown_achievements"]))

        embed.add_field(
            name=self.lang("embed.social.title"),
            value=self.lang("embed.social.formatting").format(
                profile_social=profile["social"], currency=self.bot.prefs.currency,
                needed_exp=to_level_up(profile["social"]["month_level"]), season=self.bot.prefs.current_season,
                total_level=exp_based_total_level(profile["social"]["total_exp"])),
            inline=False)

        return embed

    @commands.command(name="profile")
    async def profile(self, ctx, *, user: MemberConverter=None):
        if user is None:
            user = ctx.author
        await ctx.send(embed=self.embed_maker(safe_load(user), user))

    @profile.error
    async def profile_handler(self, ctx, error):
        if isinstance(error, BadArgument):
            if "Member" in str(error):
                return await ctx.send(self.lang("cmds.profile.bad_username").format(
                    name=str(error).lstrip('Member "').rstrip('" not found')))
        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command(name="setprofile", aliases=["editprofile", "set"])
    async def setprofile(self, ctx, field, *, value):
        settable_text_fields = self.settable_text_fields
        settable_int_fields = self.settable_int_fields
        field = field.lower().strip(" ").strip('"').strip(" ")
        value = value.strip(" ")
        if field in settable_text_fields.keys():
            profile = safe_load(ctx.author)
            if "." in settable_text_fields[field]:
                key, subkey = settable_text_fields[field].split(".")
                profile[key][subkey] = value
            else:
                profile[settable_text_fields[field]] = value
            save(profile)
            return await ctx.send(self.lang("cmds.setprofile.success").format(field=field))
        if field in settable_int_fields.keys():
            profile = safe_load(ctx.author)
            if "." in settable_int_fields[field]:
                key, subkey = settable_int_fields[field].split(".")
                try:
                    profile[key][subkey] = int(value)
                except ValueError:
                    return await ctx.send(self.lang("cmds.setprofile.not_a_number"))
            else:
                try:
                    profile[settable_int_fields[field]] = int(value)
                except ValueError:
                    return await ctx.send(self.lang("cmds.setprofile.not_a_number"))
            save(profile)
            return await ctx.send(self.lang("cmds.setprofile.success").format(field=field))
        if field in ["region"]:
            return await ctx.send(self.lang("cmds.setprofile.region").format(prefix=self.prefix))
        if field in ["platform", "platforms"]:
            return await ctx.send(self.lang("cmds.setprofile.platform").format(prefix=self.prefix))
        return await ctx.send(self.lang("cmds.setprofile.invalid_field").format(prefix=self.prefix))

    @setprofile.error
    async def setprofile_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            return await ctx.send(self.lang("cmds.setprofile.no_args").format(prefix=self.prefix))
        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command(name="region", aliases=["setregion", "regions"])
    async def region(self, ctx, *, region: str=None):
        valid_regions = {
            "asia": "Asia",
            "brazil": "Brazil",
            "br": "Brazil",
            "eu": "EU",
            "europe": "EU",
            "nae": "NA-E",
            "na-e": "NA-E",
            "na e": "NA-E",
            "na east": "NA-E",
            "naw": "NA-W",
            "na-w": "NA-W",
            "na w": "NA-W",
            "na west": "NA-W",
            "oce": "OCE",
            "oceania": "OCE"
        }
        if region is None:
            return await ctx.send(self.lang("cmds.region.regions_list").format(
                prefix=self.prefix, valid_regions=self.lang("cmds.region.valid_regions")))
        region = region.lower()
        if region not in valid_regions.keys():
            return await ctx.send(self.lang("cmds.region.invalid_region").format(
                valid_regions=self.lang("cmds.region.valid_regions")))
        region = valid_regions[region]
        profile = safe_load(ctx.author)
        profile["game"]["region"] = region
        save(profile)
        to_remove = []
        equipped = False
        for role in ctx.author.roles:
            if role.id in self.regions_roles.values():
                if role.id == self.regions_roles[region]:
                    equipped = True
                    await ctx.send(self.lang("cmds.region.role_already_selected"))
                else:
                    to_remove.append(role)
        if to_remove:
            await ctx.author.remove_roles(*to_remove, reason="User selected new region")
        if not equipped:
            await ctx.author.add_roles(discord.Object(self.regions_roles[region]), reason="User selected new region")
            await ctx.send(self.lang("cmds.region.success"))

    @commands.command(name="platform", aliases=["setplatform", "platforms"])
    async def platform(self, ctx, *, args: str=None):
        valid_platforms = {
            "pc": "PC",
            "mac": "PC",
            "xb1": "XB1",
            "xb 1": "XB1",
            "xbox one": "XB1",
            "xbox1": "XB1",
            "ps4": "PS4",
            "playstation": "PS4",
            "ps 4": "PS4",
            "ios": "iOs",
            "iphone": "iOs",
            "ipad": "iOs",
            "android": "Android",
            "samsung": "Android",
            "switch": "Switch",
            "nintendo": "Switch",
            "nintendo switch": "Switch"
        }
        if args is None:
            return await ctx.send(self.lang("cmds.platform.platforms_list").format(
                prefix=self.prefix, valid_platforms=self.lang("cmds.platform.valid_platforms")))
        args = args.lower()
        what = args.split(" ")[0]
        if what not in ["add", "set", "reset"]:
            what = "add"
        elif what == "reset":
            profile = safe_load(ctx.author)
            profile["game"]["platforms"] = []
            save(profile)
            to_remove = []
            for role in ctx.author.roles:
                if role.id in self.platform_roles.values():
                    to_remove.append(role)
            if to_remove:
                await ctx.author.remove_roles(*to_remove, reason="User issued platforms reset")
            return await ctx.send(self.lang("cmds.platform.reset_success"))

        args = args.replace(what, "").strip(" ")
        if args not in valid_platforms.keys():
            return await ctx.send(self.lang("cmds.platform.invalid_platform").format(
                prefix=self.prefix, valid_platforms=self.lang("cmds.platform.valid_platforms")))
        if what == "set":
            platform = valid_platforms[args]
            profile = safe_load(ctx.author)
            profile["game"]["platforms"] = [platform]
            save(profile)
            to_remove = []
            equipped = False
            for role in ctx.author.roles:
                if role.id in self.platform_roles.values():
                    if role.id == self.platform_roles[platform]:
                        equipped = True
                    else:
                        to_remove.append(role)
            if to_remove:
                await ctx.author.remove_roles(*to_remove, reason="User selected new platform")
            if equipped:
                return await ctx.send(self.lang("cmds.platform.role_already_selected"))
            else:
                await ctx.author.add_roles(discord.Object(self.platform_roles[platform]),
                                           reason="User selected new platform")
                return await ctx.send(self.lang("cmds.platform.set_success"))
        elif what == "add":
            platform = valid_platforms[args]
            profile = safe_load(ctx.author)
            if platform not in profile["game"]["platforms"]:
                profile["game"]["platforms"].append(platform)
                save(profile)
            for role in ctx.author.roles:
                if role.id in self.platform_roles.values():
                    if role.id == self.platform_roles[platform]:
                        return await ctx.send(self.lang("cmds.platform.role_already_added"))
            await ctx.author.add_roles(discord.Object(self.platform_roles[platform]),
                                       reason="User added new platform")
            return await ctx.send(self.lang("cmds.platform.add_success"))


def setup(bot):
    bot.add_cog(ProfilesCog(bot))
