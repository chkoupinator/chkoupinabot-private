from discord.ext import commands
from lang.lang_wrap import LangWrap
import discord
from chkoupinabot import ChkoupinaBot
from utils.converters import MemberConverter
import traceback
import sys
from utils.misc import user_to_detailed_str

lang_settings = {
    "cmds": {
        "modmail": {
            "help": {
                "name": "modmail",
                "usage": "`{prefix}modmail [-a] [-e] <message>`\nsends the `<message>` to the mods\n"
                         "`-a` makes your name hidden to the mods although Chkoupinator can still check your identity "
                         "if needed (spam/breaking rules)\n"
                         "`-e` tells calls the mod in an emergency (only usable by Helpers)",
                "short_desc": "a command to contact the mods of the server",
                "long_desc": "used to send a suggestion/report message to the mods, also allows sending "
                             "(pseudo) anonymous messages through the use of the `-a` flag"
            },
            "not_helper": "error: you cannot use the emergency option (`-e`) as the helper role has not been "
                          "found on your profile!",
            "new_modmail": "**New modmail!**",
            "emergency": "emergency wee woo call 911 mayday mayday",
            "signature": "message sent by {who}",
            "anonymous": "anonymous",
            "success": "your message has been successfully forwarded to the mods",
            "emergency_success": " and notified the Server Staff role",
            "anonymous_notice": "\nPS: since you chose to hide your identity mods are not able to reply to you, "
                                "also your identity is not completely hidden, Chkoupinator can still backtrack you "
                                 "in case of extreme situations (spamming/sending nsfw/gore/breaking the rules etc)"
        },
        "modreply": {
            "help": {
                "name": "modreply",
                "usage": "`{prefix}modreply [-a] <user> <message>`\nsends the `<message>` to the `<user>`\n"
                         "`-a` makes the author of the message anonymous",
                "short_desc": "a command for the mods to reply to the user's modmail",
                "long_desc": "used by the mods to reply to the modmails user sent (needs the Server Staff role)"
            },
            "from": "message from {who}:",
            "anonymous": "the moderation team",
            "bad_username": "error: user named `{name}` not found",
            "server_only": "error: this command can only be used from the server",
            "not_staff": "error: only members with the Server Staff role can use this command",
            "reply_header": "reply to {user} sent by {mod}:"
        }
    },
    "help": {
        "name": "moderation module",
        "short_desc": "a module that gives some neat moderation tools",
        "long_desc": "a WIP module that gives some additional moderation related tools to the bot"
    },
    "flag_not_recognized": "error: flag `-{flag}` not recognized",
}


class ModerationCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        self.modmail_channel_id = 546379620539301888
        self.modmail_channel = None  # type: discord.TextChannel
        self.helper_role_id = 448247918521090058
        self.server_staff_id = 332221421247922177

    def check_modmail_channel(self):
        if self.modmail_channel is None:
            self.modmail_channel = self.bot.get_channel(self.modmail_channel_id)
            # manual version:
            # for guild in self.bot.guilds:
            #     if guild.id == self.bot.prefs.main_guild:
            #         for channel in guild.text_channels:
            #             if channel.id == self.modmail_channel_id:
            #                 self.modmail_channel = channel
            #                 break
            #         else:
            #             print(f"Fatal Error: modmail channel not found (ID : {self.modmail_channel_id})")

        if self.modmail_channel is None:
            print(f"Fatal Error: modmail channel not found (ID : {self.modmail_channel_id})")
            return True
        else:
            return False

    @commands.command(name="modmail", aliases=["mailmod", "mod-mail", "mail_mods"])
    async def modmail(self, ctx, *, content: str=""):
        content = content.strip(" ")
        anonymous = False
        emergency = False
        while content.startswith("-"):
            content = content[1:]

            if content.startswith(" "):
                content = content.strip(" ")

            if content.startswith("a"):
                anonymous = True
            elif content.startswith("e"):
                if isinstance(ctx.author, discord.Member):
                    for role in ctx.author.roles:
                        if role.id == self.helper_role_id:
                            break
                    else:
                        return await ctx.send(self.lang("cmds.modmail.not_helper"))
                emergency = True
            else:
                return await ctx.send(self.lang("flag_not_recognized").format(flag=content[0:1]))

            content = content[1:].strip(" ")
        signature = self.lang("cmds.modmail.signature").format(
            who=self.lang("cmds.modmail.anonymous") if anonymous else user_to_detailed_str(ctx.author))

        if self.check_modmail_channel():  # if there is an error
            return

        if emergency:
            messsage = await self.modmail_channel.send(
                f'{self.lang("cmds.modmail.emergency")} <@&{self.server_staff_id}>\n'
                f'{content}\n'
                f'-- {signature}')
        else:
            messsage = await self.modmail_channel.send(
                f'{self.lang("cmds.modmail.new_modmail")}\n'
                f'{content}\n'
                f'-- {signature}')
        if anonymous:
            if not self.bot.chkoupinator:
                self.bot.chkoupinator = self.bot.get_guild(self.bot.prefs.main_guild).owner
            await self.bot.chkoupinator.send(f"message with ID {messsage.id} was originally sent by {ctx.author.id} "
                                             f"(\"{content[0:100]}...\")")
        await ctx.send(f'{self.lang("cmds.modmail.success")}'
                       f'{self.lang("cmds.modmail.emergency_success") if emergency else ""}'
                       f'{self.lang("cmds.modmail.anonymous_notice") if anonymous else ""}')

    @commands.command(name="modreply")
    async def modreply(self, ctx, target: MemberConverter, *, content: str):
        if not isinstance(ctx.author, discord.Member):
            return await ctx.send(self.lang("cmds.modreply.bad_username"))
        for role in ctx.author.roles:
            if role.id == self.server_staff_id:
                break
        else:
            return await ctx.send(self.lang("cmds.modreply.not_staff"))
        anonymous = False
        while content.startswith("-"):
            content = content[1:]
            if content.startswith(" "):
                content = content.strip(" ")

            if content.startswith("a"):
                anonymous = True
            else:
                return await ctx.send(self.lang("flag_not_recognized").format(flag=content[0:1]))
            content = content[1:].strip(" ")
        from_who = self.lang("cmds.modreply.anonymous") if anonymous else user_to_detailed_str(ctx.author)
        await target.send(f'{self.lang("cmds.modreply.from").format(who=from_who)}\n{content}')

        if self.check_modmail_channel():  # if there is an error
            return

        await self.modmail_channel.send(self.lang("cmds.modreply.reply_header").format(
            user=user_to_detailed_str(target), mod=f'{user_to_detailed_str(ctx.author)}'
                                                   f'{" (anonymously)" if anonymous else ""}') + "\n" + content)

    @modreply.error
    async def profile_handler(self, ctx, error):
        if isinstance(error, discord.ext.commands.errors.BadArgument):
            if "Member" in str(error):
                return await ctx.send(self.lang("cmds.modreply.bad_username").format(
                    name=str(error).lstrip('Member "').rstrip('" not found')))
        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)


def setup(bot):
    bot.add_cog(ModerationCog(bot))
