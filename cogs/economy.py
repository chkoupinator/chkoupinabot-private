from lang.lang_wrap import LangWrap
from discord.ext import commands
import json
import discord
from cogs import profiles
import random
import asyncio
from utils.converters import MemberConverter
from utils.exceptions import *
from chkoupinabot import ChkoupinaBot
from collections import Counter
from discord.ext.commands import Context
import traceback
import sys

lang_settings = {
    "cmds": {
        "shop": {
            "help": {
                "name": "shop",
                "usage": "`{prefix}shop` lists all purchasable roles and items, use `{prefix}buy <name>` to buy "
                         "something",
                "short_desc": "lists shop items and roles",
                "long_desc": "lists all the purchasable roles and items that can be bought"
            },
            "embed_title": "shop items",
            "role_format": "{previous}<@&{role_id}>\n name: `{item_name}` lvl: `{level}` price: `{price}`\n",
            "owned_role_format": "{previous}<@&{role_id}>\n name: `{item_name}` **owned**\n",
            "unobtainable_role_format": "{previous}<@&{role_id}>\n name: `{item_name}` **unobtainable**\n",
        },
        "buy": {
            "help": {
                "name": "buy",
                "usage": "`{prefix}buy <item name>`\n`<item name>` has to be the unique name of one of the items "
                         "in the shop that you can list using `{prefix}shop` (not case sensitive)\n"
                         "initiates the purchase of the item named `<item name>` and asks you for confirmation",
                "short_desc": "allows you to buy items from the shop",
                "long_desc": "this command looks for an item by the name you gave and asks you for confirmation "
                             "on your purchase then gives you the item if it is a successful purchase"
            },
            "item_not_found": "error: the item named `{name}` has not been found, use `{prefix}shop` to list the "
                              "shop items",
            "item_unobtainable": "error: `{name}` cannot be purchased, if you have it equip it with "
                                 "`{prefix}equip {name}` instead",
            "colour_owned": "error: you already own this colour role, equip it with `{prefix}equip {name}` instead",
            "too_low_level": "error: the item you are trying to buy (`{item[name]}`) requires you to be at least level "
                             "**{item[required_lvl]}** but your current level is  **{current}**",
            "too_expensive": "error: the item you are trying to buy (`{item[name]}`) requires "
                             "**{item[price]}**{currency} but you only have **{current_money}**{currency}...",
            "confirm": {
                "colour": "please confirm your purchase of the colour role named `{item[name]}` "
                          "(preview: <@&{item[id]}>) for **{item[price]}**{currency} by typing **`{random_int}`** in "
                          "this channel within the next minute, typing `cancel` will cancel the purchase\n"
                          "current money: **{current_money}**{currency}, after purchase: **{new_money}**{currency}"
            },
            "cancelled": "purchase cancelled <:pepe_ok_hand:382198933134245888>",
            "timeout": "timeout: you took too much time to answer, the purchase got automatically cancelled ",
            "success": "purchase successful, your current balance is now **{balance}**{currency} "
                       "<:pepe_ok_hand:382198933134245888>",
            "missing_arg": "this command needs the name of what you want to buy, use `{prefix}shop` to find "
                           "what is currently purchasable and then do `{prefix}buy <something you can buy>`"

        },
        "equip": {
            "help": {
                "name": "equip",
                "usage": "`{prefix}equip <item name>`\n`<item name>` has to be the unique name of one of the items "
                         "in the shop that you own which can be found using `{prefix}shop` or `{prefix}inventory`, "
                         "the name is not case sensitive\n"
                         "equips/uses the item named `<item name>`",
                "short_desc": "allows you to equip colour roles and use items from the shop",
                "long_desc": "this command looks for an item by the name you gave and checks if you own it then "
                             "equips it for you if you do"
            },
            "item_not_found": "error: the item named `{name}` has not been found, use `{prefix}inventory` to check the "
                              "items you own or `{prefix}shop` to see what you can purchase",
            "colour_not_owned": "error: you do not own this colour role, try buying it using `{prefix}buy {name}` "
                                "instead (if it purchasable)",
            "item_not_owned": "error: you do not own this item, try buying it using `{prefix}buy {name}` instead "
                              "(if it purchasable)",
            "colour_equipped": "the colour <@&{id}> is already equipped, the other colour roles have "
                               "been successfully unequipped",
            "colour_success": "you have successfully equipped the colour <@&{id}> <:pepe_ok_hand:382198933134245888>",
            "missing_arg": "this command needs the name of what you want to equip, use `{prefix}inventory` to find "
                           "what you currently have and do `{prefix}equip <something you have>`"

        },
        "inventory": {
            "help": {
                "name": "inventory",
                "usage": "`{prefix}inventory`\n lists all the roles and items that you own",
                "short_desc": "lists inventory items and roles",
                "long_desc": "lists all the acquired roles and items that you have, you can use `shop` to list the "
                             "items that can be purchased, `buy` to buy an item from the shop and `equip` to equip one "
                             "of the items you have in your inventory"
            },
            "embed_title": "{user.name}'s inventory",
            "colours": "**colours:**\n",
            "colours_overflow": "more colours:",
            "colour_line_format": "-`{item[name]}`: <@&{item[id]}>\n",
            "others": "other items:",
            "other_line_format": "-{item[name]}\n",
            "exp_boosts": "exp boosts:",
            "count_boosts": "**{count}x** `{name}`: {description}\n",
            "credits_coupons": "credits coupons:",
            "count_coupons": "**{set[1]}x** `{set[0]}`, ",
            "unknown": "-**error unknown or corrupted item with name `{name}`**\n",
            "target_not_found": "error: user named `{name}` not found, did you mean `{prefix}equip` instead?"
        },
        "unequip": {
            "help": {
                "name": "unequip",
                "usage": "`{prefix}unequip`\n"
                         "removes all the colour roles that you have equipped",
                "short_desc": "allows to take off all the colour roles that you have",
                "long_desc": "this command looks for all your roles and removes them if they are part of the colour "
                             "roles in the visible or hidden parts of the shop"
            },
            "success": "all your colour roles have successfully been removed <:pepe_ok_hand:382198933134245888>"
        },
        "use": {
            "help": {
                "name": "use",
                "usage": "`{prefix}use <what>`\n"
                         "`what` is the name of the item you want to use\n"
                         "Example: `{prefix}use 50 credits coupon`",
                "short_desc": "allows you to use items that you have acquired",
                "long_desc": "this command allows you to use the different items that you can acquire through rewards "
                             "or by buying them from the shop"
            },
            "no_args": "you need to specify what you want to use, for more help please type `{prefix}help use`",
            "item_not_found": "error: the item named `{name}` has not been found, use `{prefix}inventory` to check the "
                              "items you own or `{prefix}shop` to see what you can purchase",
            "colour": "please use `{prefix}equip {name}` to equip that colour instead",
            "unknown_type": "the item you are trying is of an unknown type (`{t}`). "
                            "If you think this is a bug please @ mention Chkoupinator#7777",
            "coupon_confirm": "do you want to use this `{value}{currency}` coupon on yourself?\n"
                              "answer with `yes` or `self` to use it on yourself.\n"
                              "answer with a message that @mentions the user you want to give it to if you "
                              "want to give it to someone else.",
            "cancelled": "operation cancelled",
            "coupon_gift_success": "the {value}{currency} coupon has been used on {who} successfully (from {sender})!",
            "coupon_use_success": "the {value}{currency} coupon has been successfully claimed!",
            "duration_unknown": "the duration of your exp boost was not recognized",
            "exp_boost_confirm": "do you want to use this `+{value}% {duration} exp boost` on yourself?\n"
                                 "answer with `yes` or `self` to use it on yourself.\n"
                                 "answer with a message that @mentions the user you want to give it to if you "
                                 "want to give it to someone else (will double it's boost and possibly the amount of "
                                 "messages it stays).",
            "exp_boost_gift_success": "the +{value:.0%} {duration} exp boost has been activated on {who} successfully "
                                      "(from {sender})!",
            "exp_boost_use_success": "the +{value:.0%}  {duration} exp boost has been successfully activated!",
            "exp_boosts_maxed": "exp boost max reached (200% per type)",
            "item_not_owned": "you do not own the item you are trying to use..."

        }
    },
    "help": {
        "name": "economy module",
        "short_desc": "contains the shop and coloured roles ",
        "long_desc": "the economy module that contains the commands to list, buy and equip the items of the shop"
    }
}


# shop_template = {
#     {
#         "Category 1": {
#             "item1": {
#                 "price": 0,
#                 "required_lvl": 0,
#                 "id": None
#             },
#             "role1": {
#                 "price": 0,
#                 "required_lvl": 0,
#                 "id": 42624624214624624
#             }
#         },
#         "Category 2": {
#             "item1": {
#                 "id": None
#             },
#             "item2": {
#                 "id": None
#             },
#             "default_price": 0,
#             "default_required_lvl": 0
#         }
#     }
# }


def load_shop():
    with open("shop.json", 'r') as shop_file:
        shop = json.load(shop_file)
        for cat in shop.values():
            for key, item in cat.items():
                if not isinstance(item, dict):
                    continue
                item["name"] = key
                if "price" not in item.keys():
                    item["price"] = cat["default_price"]
                if "required_lvl" not in item.keys():
                    item["required_lvl"] = cat["default_required_lvl"]
                if "type" not in item.keys():
                    item["type"] = cat["default_type"]
        return shop


def get_colour_ids(shop):
    ids_list = list()

    for cat in shop.keys():
        for item in shop[cat].keys():
            if not isinstance(shop[cat][item], dict):
                continue
            if shop[cat][item]["type"] == "colour":
                ids_list.append(shop[cat][item]["id"])
    return ids_list


def make_name_to_item(shop):
    name_to_item = dict()
    for cat in shop.keys():
        for item in shop[cat].keys():
            if not isinstance(shop[cat][item], dict):
                continue
            name_to_item[item.strip(' ').strip('"').strip(' ').lower()] = shop[cat][item]
    return name_to_item


def remove_item(name: str, inventory: list):
    for i in range(len(inventory)):
        if inventory[i].lower() == name:
            del inventory[i]
            return True
    return False


class EconomyCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        self.shop_d = load_shop()
        self.colour_ids = get_colour_ids(self.shop_d)
        self.name_to_item = make_name_to_item(self.shop_d)

    def reload_shop(self):
        self.shop_d = load_shop()

    def reload_colour_ids(self):
        self.colour_ids = get_colour_ids(self.shop_d)

    def reload_name_to_item(self):
        self.name_to_item = make_name_to_item(self.shop_d)

    def get_item_from_name(self, name: str):
        return self.name_to_item[name.strip(' ').strip('"').strip(' ').lower()]

    @commands.command(name="shop", aliases=["shoplist"])
    async def shop(self, ctx: commands.Context):
        profile = profiles.safe_load(ctx.author)
        embed = discord.Embed(title=self.lang("cmds.shop.embed_title"))
        for category in self.shop_d.keys():
            if self.shop_d[category]["hidden"]:
                continue
            m = str()
            for item in self.shop_d[category].keys():
                if not isinstance(self.shop_d[category][item], dict):
                    continue
                state = 0  # 0 = purchasable,  1 = owned,  -1 = unobtainable
                i = self.shop_d[category][item]
                if item in profile["inventory"]["colour_roles"] or item in profile["inventory"]["other"]:
                    state = 1
                elif i['price'] is None:
                    state = -1

                if self.shop_d[category][item]["type"] == "colour":
                    if state == 1:  # TODO add support for items that can be purchased multiple times
                        m = self.lang("cmds.shop.owned_role_format").format(previous=m, item_name=item, role_id=i["id"])
                    elif state == -1:
                        m = self.lang("cmds.shop.unobtainable_role_format").format(previous=m, item_name=item,
                                                                                   role_id=i["id"])
                    else:
                        m = self.lang("cmds.shop.role_format").format(previous=m, item_name=item, role_id=i["id"],
                                                                      level=i["required_lvl"],
                                                                      price=f"{i['price']}{self.bot.prefs.currency}", )

            embed.add_field(name=category, value=m)
        await ctx.send(embed=embed)

    @commands.command(name="buy", aliases=["purchase", "shopbuy", "buyrole"])
    async def buy(self, ctx: commands.Context, *, what: str):
        try:
            item = self.name_to_item[what.strip(' ').strip('"').strip(' ').lower()]
        except KeyError:
            return await ctx.send(self.lang("cmds.buy.item_not_found").format(name=what, prefix=self.prefix))
        if item["price"] is None or item["required_lvl"] is None:
            return await ctx.send(self.lang("cmds.buy.item_unobtainable").format(name=item["name"], prefix=self.prefix))

        profile = profiles.safe_load(ctx.author)

        user_level = profiles.exp_based_total_level(profile["social"]["total_exp"])

        if item["type"] == "colour":
            if item["name"] in profile["inventory"]["colour_roles"]:
                return await ctx.send(self.lang("cmds.buy.colour_owned").format(name=item["name"], prefix=self.prefix))
        if user_level < item["required_lvl"]:
            return await ctx.send(self.lang("cmds.buy.too_low_level").format(item=item, current=user_level))
        if profile["social"]["currency"] < item["price"]:
            return await ctx.send(self.lang("cmds.buy.too_expensive").format(
                item=item, currency=self.bot.prefs.currency, current_money=profile["social"]["currency"]))

        random_int = f"{random.randrange(1, 10)}{random.randrange(1, 10)}{random.randrange(1, 10)}" \
                     f"{random.randrange(1, 10)}"
        confirmation_message = await ctx.send(self.lang(f"cmds.buy.confirm.{item['type']}").format(
            item=item, currency=self.bot.prefs.currency, current_money=profile["social"]["currency"],
            new_money=profile["social"]["currency"] - item["price"], random_int=random_int))  # type: discord.Message

        def check(m):
            if m.author == ctx.author and m.channel == ctx.channel:
                return m.content.lower() in ["cancel", random_int]
            return False

        try:
            answer = await self.bot.wait_for('message', timeout=60, check=check)  # type: discord.Message
            if answer.content.lower() == "cancel":
                await ctx.send(self.lang("cmds.buy.cancelled"))
                return await confirmation_message.delete()
        except asyncio.TimeoutError:
            await ctx.send(self.lang("cmds.buy.timeout"))
            return await confirmation_message.delete()

        profile = profiles.safe_load(ctx.author)
        profile["social"]["currency"] -= item["price"]
        if item["type"] == "colour":
            profile["inventory"]["colour_roles"].append(item["name"])
        else:
            profile["inventory"]["other"].append(item["name"])
        profile["tracking"]["used_currency"] += item["price"]
        profile["tracking"]["purchases_made"] += 1
        # TODO add check for achievement unlocked
        profiles.save(profile)
        await ctx.send(self.lang("cmds.buy.success").format(currency=self.bot.prefs.currency,
                                                            balance=profile["social"]["currency"]))

    @buy.error
    async def buy_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(self.lang("cmds.buy.missing_arg").format(prefix=self.prefix))

    @commands.command(name="equip", aliases=["colour", "color"])
    async def equip(self, ctx: commands.Context, *, what: str):
        try:
            item = self.name_to_item[what.strip(' ').strip('"').strip(' ').lower()]
        except KeyError:
            return await ctx.send(self.lang("cmds.equip.item_not_found").format(name=what, prefix=self.prefix))
        profile = profiles.safe_load(ctx.author)
        if item["type"] == "colour":
            if item["name"] not in profile["inventory"]["colour_roles"]:
                return await ctx.send(self.lang("cmds.equip.colour_not_owned").format(name=item["name"],
                                                                                      prefix=self.prefix))
        else:
            if item["name"] not in profile["inventory"]["other"]:
                return await ctx.send(self.lang("cmds.equip.item_not_owned").format(name=item["name"],
                                                                                    prefix=self.prefix))
        if item["type"] == "colour":
            to_delete = list()
            equipped = False
            for role in ctx.author.roles:
                if role.id in self.colour_ids:
                    if role.id == item["id"]:
                        equipped = True
                    else:
                        to_delete.append(role)

            await ctx.author.remove_roles(*to_delete, reason="User equipped a new colour role")
            if equipped:
                await ctx.send(self.lang("cmds.equip.colour_equipped").format(id=item["id"]))
            else:
                await ctx.author.add_roles(discord.Object(id=item["id"]), reason="User equipped a new colour role")
                await ctx.send(self.lang("cmds.equip.colour_success").format(id=item["id"]))

    # TODO add other item types than roles to the shop
    # using exp boosts should never make the user go above 200% bonus monthly exp and bonus daily exp
    # (for a max of 400%) using it on self gives half of the boost
    # credit coupons can be used on self or gifted and give the same amount

    @equip.error
    async def equip_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            await ctx.send(self.lang("cmds.equip.missing_arg").format(prefix=self.prefix))

    @commands.command(name="unequip", aliases=["resetcolor", "resetcolours"])
    async def unequip(self, ctx: commands.Context):
        to_delete = list()
        for role in ctx.author.roles:
            if role.id in self.colour_ids:
                to_delete.append(role)

        await ctx.author.remove_roles(*to_delete, reason="User unequipped all colour roles")
        await ctx.send(self.lang("cmds.unequip.success"))

    @commands.command(name="inventory", aliases=["inv"])
    async def inventory(self, ctx, *, user: MemberConverter = None):
        if user is None:
            user = ctx.author  # type: discord.Member
        profile = profiles.safe_load(user)
        overflow = ""
        items = self.lang("cmds.inventory.colours")
        mention_spoop = False
        for item_name in profile["inventory"]["colour_roles"]:
            try:
                item = self.get_item_from_name(item_name)
                items = f'{items}{self.lang("cmds.inventory.colour_line_format").format(item=item)}'
            except KeyError:
                items = f'{items}{self.lang("cmds.inventory.unknown").format(name=item_name)}'
                mention_spoop = True
        if len(items) > 1900:
            lines = items.split("\n")
            items = lines[0]
            i = 1
            while i < len(lines):
                buffer = f'{items}\n{lines[i]}'
                if len(buffer) > 1900:
                    break
                else:
                    items = buffer
                i += 1
            while i < len(lines):
                overflow = f'{overflow}\n{lines[i]}'
                i += 1

        embed = discord.Embed(title=self.lang("cmds.inventory.embed_title").format(user=user), description=items,
                              colour=user.colour.value if isinstance(user, discord.Member) else 0x000)
        if overflow:
            embed.add_field(name=self.lang("cmds.inventory.colours_overflow"), value=overflow)
        if profile["inventory"]["other"]:
            other_items = str()
            monthly_exp_boosts = []
            daily_exp_boosts = []
            coupons = []
            for item_name in profile["inventory"]["other"]:  # type: str
                if "Exp Boost" in item_name:
                    if "Daily" in item_name:
                        daily_exp_boosts.append(item_name)
                    elif "Monthly" in item_name:
                        monthly_exp_boosts.append(item_name)
                elif "Credits Coupon" in item_name:
                    coupons.append(item_name.replace("  Credits Coupon", ""))
                else:
                    other_items = f'{other_items}{self.lang("cmds.inventory.other_line_format").format(item=item_name)}'
            if other_items:
                embed.add_field(name=self.lang("cmds.inventory.others"), value=other_items)
            if monthly_exp_boosts or daily_exp_boosts:
                exp_boosts_str = str()
                exp_boosts = Counter(monthly_exp_boosts + daily_exp_boosts).most_common()
                for exp_boost in exp_boosts:
                    name = exp_boost[0]
                    line = self.lang("cmds.inventory.count_boosts").format(
                        count=exp_boost[1], name=name, description=self.shop_d["Exp Boosts"][name]["description"])
                    exp_boosts_str = f'{exp_boosts_str}{line}'
                embed.add_field(name=self.lang("cmds.inventory.exp_boosts"), value=exp_boosts_str)
            if coupons:
                coupons_str = str()
                coupons = Counter(coupons).most_common()
                for coupon_set in coupons:
                    coupons_str = f'{coupons_str}{self.lang("cmds.inventory.count_coupons").format(set=coupon_set)}'
                embed.add_field(name=self.lang("cmds.inventory.credits_coupons"), value=coupons_str)

        if mention_spoop:
            await ctx.send("<@167122174467899393>", embed=embed)
        else:
            await ctx.send(embed=embed)

    @inventory.error
    async def inventory_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            if "Member" in str(error):
                return await ctx.send(self.lang("cmds.inventory.target_not_found").format(
                    name=str(error).lstrip('Member "').rstrip('" not found'), prefix=self.prefix))
        elif isinstance(error, WrongChannel):
            return
        else:
            print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
            traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command(name="use")
    async def use(self, ctx: Context, *, what: str = None):
        # return await ctx.send("This command has been disabled while a critical bug gets fixed")
        # DONE: fix the bug with items not being removed from inventory on use
        if what is None:
            return await ctx.send(self.lang("cmds.use.no_args").format(prefix=self.prefix))
        what = what.lower().strip(' ').strip('"').strip(' ')
        try:
            item = self.get_item_from_name(what)
        except KeyError:
            return await ctx.send(self.lang("cmds.use.item_not_found").format(name=what, prefix=self.prefix))
        if item["type"] == "colour":
            return await ctx.send(self.lang("cmds.use.colour").format(name=what, prefix=self.prefix))
        if item["type"] == "coupon":
            currency = self.bot.prefs.currency
            value = int(what.strip(' ').strip('"').strip(' ').split(" ")[0])
            await ctx.send(self.lang("cmds.use.coupon_confirm").format(value=value, currency=currency))

            def check(m):
                if m.author == ctx.author and m.channel == ctx.channel:
                    return True

            try:
                answer = await self.bot.wait_for('message', timeout=60, check=check)  # type: discord.Message
                if answer.content.lower() in ["self", "yes"]:
                    who = ctx.author
                elif len(answer.mentions) == 1:
                    who = answer.mentions[0]  # type: discord.Member
                else:
                    return await ctx.send(self.lang("cmds.use.cancelled"))
            except asyncio.TimeoutError:
                return await ctx.send(self.lang("cmds.use.cancelled"))
            sender_profile = profiles.load(ctx.author.id)
            removed = remove_item(what, sender_profile["inventory"]["other"])
            if not removed:
                return await ctx.send(self.lang("cmds.use.item_not_owned"))
            if who != ctx.author:
                profile = profiles.load(who.id)
            else:
                profile = sender_profile
            profile["social"]["currency"] += value
            profile["tracking"]["total_currency"] += value
            if who != ctx.author:
                profile["tracking"]["received_currency_gifts"] += value
                sender_profile["tracking"]["sent_currency_gifts"] += value
                profiles.save(sender_profile)
                profiles.save(profile)
                return await ctx.send(self.lang("cmds.use.coupon_gift_success").format(
                    value=value, currency=currency, who=who.mention, sender=ctx.author.mention))
            else:
                profiles.save(profile)
                return await ctx.send(self.lang("cmds.use.coupon_use_success").format(value=value, currency=currency))
        elif item["type"] == "exp_boost":
            value = int(what.split(" ")[0].replace("+", "").replace("%", ""))
            if "monthly" in what:
                duration = "monthly"
            elif "daily" in what:
                duration = "daily"
            else:
                return await ctx.send(self.lang("cmds.use.duration_unknown"))
            await ctx.send(self.lang("cmds.use.exp_boost_confirm").format(
                value=value, duration=duration if duration == "monthly" else f'{duration} for 50 messages'))

            def check(m):
                if m.author == ctx.author and m.channel == ctx.channel:
                    return True

            try:
                answer = await self.bot.wait_for('message', timeout=60, check=check)  # type: discord.Message
                if answer.content.lower() in ["self", "yes"]:
                    who = ctx.author
                elif len(answer.mentions) == 1:
                    who = answer.mentions[0]  # type: discord.Member
                    value *= 2
                else:
                    return await ctx.send(self.lang("cmds.use.cancelled"))
            except asyncio.TimeoutError:
                return await ctx.send(self.lang("cmds.use.cancelled"))

            value /= 100
            profile = profiles.load(who.id)
            if profile["social"][f"{duration}_exp_multiplier"] >= 2:
                return await ctx.send(self.lang("cmds.use.exp_boosts_maxed"))
            if who == ctx.author:
                sender_profile = profile
            else:
                sender_profile = profiles.load(ctx.author.id)
            removed = remove_item(what, sender_profile["inventory"]["other"])
            if not removed:
                return await ctx.send(self.lang("cmds.use.item_not_owned"))

            profile["social"][f"{duration}_exp_multiplier"] += value
            if profile["social"][f"{duration}_exp_multiplier"] >= 2:
                profile["social"][f"{duration}_exp_multiplier"] = 2

            if who != ctx.author:
                if duration == "daily":
                    profile["social"][f"daily_boost_messages_left"] += 100
                sender_profile["tracking"]["exp_boosts_gifted"] += 1
                profile["tracking"]["exp_boosts_received"] += 1
                profiles.save(sender_profile)
                profiles.save(profile)
                return await ctx.send(self.lang("cmds.use.exp_boost_gift_success").format(
                    value=value, duration=duration, who=who.mention, sender=ctx.author.mention))
            else:
                if duration == "daily":
                    profile["social"][f"daily_boost_messages_left"] += 50
                profile["tracking"]["exp_boosts_used"] += 1
                profiles.save(profile)
                return await ctx.send(self.lang("cmds.use.exp_boost_use_success").format(value=value,
                                                                                         duration=duration))
        else:
            return await ctx.send(self.lang("cmds.use.unknown_type").format(t=item["type"]))

def setup(bot):
    bot.add_cog(EconomyCog(bot))
