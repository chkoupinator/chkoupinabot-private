from lang.lang_wrap import LangWrap
from cogs import profiles, achievements
import re
import time
import asyncio
from utils import persistence, stats
from discord.ext import commands
import discord
from utils.converters import MemberConverter
import sys
import os
import glob
import traceback
from chkoupinabot import ChkoupinaBot


lang_settings = {
    "cmds": {
        "daily": {
            "received": "you received {amount}{currency}!",
            "given": "{target} received {amount}{currency} from {user}!",
            "cooldown": "sorry, your daily has already been claimed today, come back in {hours}h {minutes}min!",
            "ongoing_streak": "you're on a roll! current multiplier {multiplier}, {advancement}/7 for the next "
                              "multiplier! total consecutive days: {total_streak}",
            "streak_lost": "you just lost your previous streak of {streak} for not claiming your daily two days in a "
                           "row. your streak has been reset to 1",
            "multiplier_gift_bonus": "{multiplier} (+1 for gifting your daily)",
            "help": {
                "name": "daily",
                "usage": "`{prefix}daily [user]`\n"
                         "`[user]` can be a nickname, a username, a username#disc, an @mention or "
                         "the id of a user (sorted from highest to lowest chance of error in the person)\n"
                         "gives your daily to the [user], get it for yourself if no user is given",
                "short_desc": "a command that can be used daily to earn currency",
                "long_desc": "a command that can be used daily to earn currency, using it on a user gives them more "
                             "than it would give if used on yourself, how much it gives can be increased by using it "
                             "every day and going on a streak (1x bonus for every week)"
            }
        }
        # "level": {
        #     "help": {
        #         "name": "level",
        #         "usage": "`{prefix}level [user]`\n"
        #                  "`[user]` can be a nickname, a username, a username#disc, an @mention or "
        #                  "the id of a user (sorted from highest to lowest chance of error in the person)\n"
        #                  "shows you the level of `[user]`, shows yours if no user is given",
        #         "short_desc": "shows the level and exp of someone",
        #         "long_desc": "a command that can be used to see the levels and the exp of someone"
        #     },
        #     "desc_formatting": "**__current month:__**\n"
        #                        "**level:** {profile_social[month_level]}\n"
        #                      "**exp:** {profile_social[level_exp]}/{needed_exp} (total {profile_social[month_exp]})\n"
        #                        "**__all time:__**\n"
        #                        "**level:** {profile_social[total_level]}\n"
        #                        "**exp:** {profile_social[total_exp]}\n"
        # }

    },
    "unlocked": "and unlocked ",
    "userlist_role": "the <@&{id}> role",
    "coupon": "a {credits}{currency} coupon",
    "monthly_exp_boost": "a monthly exp boost (not activated)",
    "daily_exp_boost": "a daily exp boost (not activated)",
    "achievement_colour_role": "the level {level} achievement for {season} with the `{colour_role}` colour role",
    "first": "you're also the first to reach this level!",
    "first_achievement": "you're also the first to get this achievement! Please contact <@167122174467899393> to "
                         "decide of the colour of the achievement colour role (if you didn't decide for one before)",
    "level_without_total": "{profile[social][month_level]}",
    "level_with_total": "{profile[social][month_level]} (sum {profile[social][total_level]}, total {total_level})",
    "level_up": "{user.mention} just leveled up to level {level} {rewards}!",
    "voice_level_up": "you just leveled up to level {level} {rewards} from using the voice channels!",
    "welcome": "Welcome to the official /r/Fortnite Discord Server, {mention}! \n"
               "We hope that you will have a great time here and for that you are invited to read the rules in "
               "<#275321943085809674> and accept them by typing `{prefix}member` in <#463162195342131230>.\n"
               "You are also welcome to get roles depending on what you play to access the channels for that! "
               "Use `{prefix}roles` in <#463162195342131230> to see the roles that you can get!\n"
               "If you are lost or have questions regarding the way the server works please head to "
               "<#335971981641908224> and <#464693571857940480> before asking your question in <#337381568060588052>.",
    "help": {
        "name": "social module",
        "short_desc": "the module that contains the social functionalities",
        "long_desc": "the module that handles the social features of the bot like levels, daily rewards etc"
    }
}


def add_exp(profile, amount, voice=False):
    if profile["social"]["daily_exp_multiplier"] > 1:
        if profile["social"]["daily_boost_messages_left"] <= 0:
            profile["social"]["daily_exp_multiplier"] = 1
    exp = profile["social"]["daily_exp_multiplier"] * profile["social"]["monthly_exp_multiplier"] * amount
    if profile["social"]["daily_exp_multiplier"] > 1:
        profile["social"]["daily_boost_messages_left"] -= 1
    profile["social"]["total_exp"] += exp
    profile["social"]["month_exp"] += exp
    if voice:
        profile["social"]["exp_from_vc_today"] += exp
    level_exp = profile["social"]["level_exp"] + exp
    exp_to_level_up = profiles.to_level_up(profile["social"]["month_level"])
    if level_exp >= exp_to_level_up:
        profile["social"]["level_exp"] = level_exp - exp_to_level_up
        profile["social"]["month_level"] += 1
        profile["social"]["total_level"] += 1
        profile["social"]["currency"] += profile["social"]["month_level"] * 50 + 50
        profile["tracking"]["total_currency"] += profile["social"]["month_level"] * 50 + 50
        return profile["social"]["month_level"]
    else:
        profile["social"]["level_exp"] = level_exp
        return False


class SocialCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        bot.social = self
        self.emoji_finder = re.compile("( *<a*:\w+:\d+> *)")
        self.loop_running = False
        self.banned_vc = [128913610918461441]
        self.banned_tc = [275320521338519552, 463162195342131230]
        self.userlist_roles = {
            1: 562289166939979806,
            5: 562289331029409796,
            10: 562289435836809225,
            25: 562289518137311262,
            50: 562289606834389024,
            75: 562289697347600394,
            100: 562289792960954368
        }
        self.currency_cards = {
            5: 500,
            10: 1000,
            15: 500,
            20: 1000,
            25: 750,
            30: 1500,
            35: 1000,
            40: 2000,
            45: 2500,
            50: 5000,
            75: 10000,
            100: 50000
        }
        self.monthly_exp_boosts = [3, 9, 15, 21, 27, 33, 39, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100]

        self.daily_exp_boosts = [7, 14, 21, 28, 35, 42, 49, 56, 63, 70, 77, 84, 91, 98]

        self.current_season = self.bot.prefs.current_season
        self.achievement_rewards = {
            1: ["Spectator", 0],
            25: ["Challenger", 1],
            50: ["Champion", 2],
            100: ["Legend", 3]
        }
        self.welcome_channel_id = 127458296243290112
        self.welcome_channel = None  # type: discord.TextChannel
        self.firsts = None

    @property
    def exp_cooldown(self):
        return self.bot.prefs.exp_cooldown

    def formatted_level(self, profile):
        if profile["social"]["total_level"] == profile["social"]["month_level"]:
            return self.lang("level_without_total").format(profile=profile)
        else:
            return self.lang("level_with_total").format(profile=profile,
                                                        total_level=
                                                        profiles.exp_based_total_level(profile["social"]["total_exp"]))

    async def level_up_rewards(self, member: discord.Member, level, reason):
        rewards = list()
        profile = None
        first = False
        if self.firsts is None:
            pers = persistence.load()
            if "firsts" not in pers.keys():
                self.firsts = dict()
                persistence.save("firsts", self.firsts)
            else:
                self.firsts = pers["firsts"]
        if str(level) not in self.firsts.keys():
            first = True
            self.firsts[f"{level}"] = member.id
            persistence.save("firsts", self.firsts)

        if level in self.userlist_roles.keys():
            to_remove = list()
            for role in member.roles:
                if role.id in self.userlist_roles.values():
                    to_remove.append(role)
            await member.remove_roles(*to_remove, reason=reason)
            await member.add_roles(discord.Object(self.userlist_roles[level]), reason=reason)
            rewards.append(self.lang("userlist_role").format(id=self.userlist_roles[level]))

        if level in self.currency_cards.keys():
            profile = profiles.safe_load(member)
            coupon = f"{self.currency_cards[level]} Credits Coupon"
            profile["inventory"]["other"].append(coupon)
            rewards.append(self.lang("coupon").format(credits=self.currency_cards[level],
                                                      currency=self.bot.prefs.currency))
        if level in self.monthly_exp_boosts:
            if profile is None:
                profile = profiles.safe_load(member)
            profile["inventory"]["other"].append("+5% Monthly Exp Boost")
            rewards.append(self.lang("monthly_exp_boost"))

        if level in self.daily_exp_boosts:
            if profile is None:
                profile = profiles.safe_load(member)
            profile["inventory"]["other"].append("+50% Daily Exp Boost")
            rewards.append(self.lang("daily_exp_boost"))

        if level in self.achievement_rewards.keys():
            if profile is None:
                profile = profiles.safe_load(member)
            colour_role = f"{self.current_season} {self.achievement_rewards[level][0]}"
            profile["inventory"]["colour_roles"].append(colour_role)
            profile["achievements"].append(achievements.unlocked(
                self.current_season.lower().replace(" ", "_"), self.achievement_rewards[level][1]))
            rewards.append(self.lang("achievement_colour_role").format(
                level=self.achievement_rewards[level][1], season=self.current_season, colour_role=colour_role))
            if first:
                profile["achievements"].append(achievements.unlocked(
                    f'{self.current_season.lower().replace(" ", "_")}_first', self.achievement_rewards[level][1]))
                rewards.append(self.lang("first_achievement"))
        elif first:
            rewards.append(self.lang("first"))

        if profile is not None:
            profiles.save(profile)
        if len(rewards) > 1:
            return f'{self.lang("unlocked")}: {" and ".join([", ".join(rewards[0:-1]), rewards[-1]])}'
        elif len(rewards) == 1:
            return f'{self.lang("unlocked")} {rewards[0]}'
        else:
            return ""

    async def give_voice_exp(self, member: discord.Member):
        profile = profiles.safe_load(member)
        if profile["social"]["max_voice_exp_hit_today"]:
            return
        level_up = False
        if profile["social"]["exp_from_vc_today"] >= 100:
            profile["tracking"]["max_voice_exp_hit"] += 1
            profile["social"]["max_voice_exp_hit_today"] = True
        else:
            level_up = add_exp(profile, 1, voice=True)
        profile["tracking"]["time_in_voice"] += 30
        profiles.save(profile)
        if level_up and not member.bot:
            rewards = await self.level_up_rewards(member, level_up, "User leveled up from voice chat")
            await member.send(self.lang("voice_level_up").format(level=self.formatted_level(profile), rewards=rewards))

    async def give_chat_exp(self, msg: discord.Message):
        profile = profiles.safe_load(msg.author)
        profile_social = profile["social"]

        try:
            profile_social["activity_per_channel_today"][str(msg.channel.id)] += 1
        except KeyError:
            profile_social["activity_per_channel_today"][str(msg.channel.id)] = 1
        try:
            profile_social["activity_per_channel_month"][str(msg.channel.id)] += 1
        except KeyError:
            profile_social["activity_per_channel_month"][str(msg.channel.id)] = 1
        if len(msg.content) > 200:
            profile_social["quality_messages_today"] += 1
            profile_social["quality_messages"] += 1
        profile_social["messages_today"] += 1
        profile_social["messages"] += 1

        if msg.author.bot:
            profiles.save(profile)
            return

        level_up = False

        if time.time() - profile_social["last_message"] > self.exp_cooldown:
            level_up = add_exp(profile, 1)

        profile_social["last_message"] = time.time()
        profiles.save(profile)
        if level_up:
            rewards = await self.level_up_rewards(msg.author, level_up,
                                                  f"User level up from sending a message in #{msg.channel.name}")
            await msg.channel.send(self.lang("level_up").format(user=msg.author, level=self.formatted_level(profile),
                                                                rewards=rewards))

    async def on_ready(self):
        self.bot.main_guild = self.bot.get_guild(self.bot.prefs.main_guild)
        if self.loop_running:
            return
        else:
            while True:
                try:
                    await self.loop()
                except Exception as error:
                    print(f"loop crashed with error {error}, restarting the loop", file=sys.stderr)
                    traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)
                    await asyncio.sleep(30)

    async def loop(self):
        while True:
            self.loop_running = True
            try:
                last_check = persistence.get("last_check")
            except KeyError:
                last_check = 0
            if time.gmtime(last_check).tm_mon != time.gmtime().tm_mon and time.gmtime().tm_mon in [1, 4, 7, 10]:
                for file_name in glob.glob(os.path.join(".", "profiles", "*.json")):  # type: str
                    userid = file_name.replace(".json", "").replace(os.path.join(".", "profiles", ""), "")
                    profile = profiles.load(userid)

                    # these two are only here to fix the shit that I put myself into:
                    # profiles.jul_sept_fallback(profile, self.bot.main_guild)
                    # profiles.oct_dec_achievements_mess(profile)
                    pers = persistence.load()
                    if "firsts" in pers.keys():
                        persistence.save(f"firsts_{self.bot.prefs.previous_season}", pers["firsts"])
                    self.firsts = dict()
                    persistence.save("firsts", self.firsts)

                    profiles.monthly_backup(profile, self.bot.prefs.previous_season)
                    profile = profiles.monthly_reset(profile)

                    profile = profiles.daily_reset(profile)

                    profiles.save(profile)
                    # TODO add a message that shows the leaderboards and gives a bounty to the highest level players
            elif time.gmtime(last_check).tm_yday != time.gmtime().tm_yday:
                for file_name in glob.glob(os.path.join(".", "profiles", "*.json")):
                    userid = file_name.replace(".json", "").replace(os.path.join(".", "profiles", ""), "")
                    profile = profiles.load(userid)
                    profile = profiles.daily_reset(profile)
                    profiles.save(profile)
                    # TODO add an option to get reminded on the daily refresh
            try:
                members_in_vc = persistence.get("members_in_vc")
            except KeyError:
                members_in_vc = []
            members_in_vc_now = []
            for voice_channel in self.bot.main_guild.voice_channels:
                if len(voice_channel.members) <= 1 or voice_channel.id in self.banned_vc:
                    continue
                for member in voice_channel.members:
                    if member.voice.deaf or member.voice.self_deaf:
                        continue
                    try:
                        members_in_vc_now.append(member.id)
                        if member.id in members_in_vc:
                            await self.give_voice_exp(member)
                        else:
                            members_in_vc.append(member.id)
                    except Exception as error:
                        print('Ignoring exception in iteration over {0.name}\'s profile (id {0.id}) when checking '
                              'members in voice chat:'.format(member), file=sys.stderr)
                        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)
                members_in_vc = members_in_vc_now
            persistence.save("members_in_vc", members_in_vc_now)
            persistence.save("last_check", time.time())
            await asyncio.sleep(30)

    async def on_message(self, msg: discord.Message):
        if msg.guild is None:
            return
        if msg.guild.id != self.bot.prefs.main_guild:
            return
        if msg.channel.id in self.banned_tc:
            return
        s = stats.load()
        try:
            today = s[time.strftime("messages-%Y-%m-%d", time.gmtime())]
        except KeyError:
            today = s[time.strftime("messages-%Y-%m-%d", time.gmtime())] = dict()
        try:
            today[str(msg.channel.id)] += 1
        except KeyError:
            today[str(msg.channel.id)] = 1
        stats.save(s)
        if not self.emoji_finder.sub("", msg.content):
            return
        if msg.content.lower() in ["lol", "lel", "hehe", "lmao", "lmfao" "xd", "no u"] or len(msg.content) < 2:
            return
        await self.give_chat_exp(msg)

    async def on_member_join(self, member: discord.Member):
        if member.guild.id != self.bot.prefs.main_guild:
            return
        if self.welcome_channel is None:
            self.welcome_channel = discord.utils.get(member.guild.text_channels, id=self.welcome_channel_id)  # type: discord.TextChannel

        await self.welcome_channel.send(self.lang("welcome").format(mention=member.mention, prefix=self.prefix))

    @commands.command(name="daily")
    async def daily(self, ctx, *, someone: MemberConverter=None):
        def lang(path, *, language=None):
            return self.lang("cmds.daily."+path, language=language)
        if someone is None:
            someone = ctx.author
            target_profile = giver_profile = profiles.safe_load(ctx.author)
        else:
            target_profile = profiles.safe_load(someone)
            giver_profile = profiles.safe_load(ctx.author)
        if giver_profile["social"]["daily_available"] > 0:
            missed = giver_profile["social"]["daily_available"]
            previous_streak = giver_profile["social"]["daily_streak"]
            if missed > 2 and giver_profile["social"]["daily_streak"] > 1:
                if giver_profile["social"]["daily_streak"] <= 8:
                    giver_profile["social"]["daily_streak"] = 1
                else:
                    giver_profile["social"]["daily_streak"] -= 7
            else:
                giver_profile["social"]["daily_streak"] += 1
            giver_profile["social"]["daily_available"] = 0
            if someone == ctx.author:
                amount = 50 * (giver_profile["social"]["daily_streak"] // 7 + 1)
                target_profile["social"]["currency"] += amount
                target_profile["tracking"]["total_currency"] += amount
            else:
                amount = 50 * (giver_profile["social"]["daily_streak"] // 7 + 2)
                target_profile["social"]["currency"] += amount
                target_profile["tracking"]["total_currency"] += amount
            if someone != ctx.author:
                profiles.save(giver_profile)
            profiles.save(target_profile)
            if someone == ctx.author:
                if missed > 2 and giver_profile["social"]["daily_streak"] > 1:
                    await ctx.send(
                        lang("received").format(amount=amount, currency=self.bot.prefs.currency)+"\n" +
                        lang("streak_lost").format(streak=previous_streak))
                else:
                    if giver_profile["social"]["daily_streak"] > 1:
                        await ctx.send(
                            lang("received").format(amount=amount, currency=self.bot.prefs.currency)+"\n" +
                            lang("ongoing_streak").format(advancement=giver_profile["social"]["daily_streak"] % 7,
                                                          multiplier=giver_profile["social"]["daily_streak"] // 7 + 1,
                                                          total_streak=giver_profile["social"]["daily_streak"]))
                    else:
                        await ctx.send(lang("received").format(amount=amount, currency=self.bot.prefs.currency)+"\n")

            else:
                if missed > 2 and giver_profile["social"]["daily_streak"] > 1:
                    await ctx.send(
                        lang("given").format(target=someone.mention, amount=amount, currency=self.bot.prefs.currency,
                                             user=ctx.author.mention) + "\n" +
                        lang("streak_lost").format(streak=previous_streak))
                elif giver_profile["social"]["daily_streak"] > 1:
                        await ctx.send(
                            lang("given").format(target=someone.mention, currency=self.bot.prefs.currency,
                                                 amount=amount, user=ctx.author.mention) + "\n" +
                            lang("ongoing_streak").format(
                                advancement=giver_profile["social"]["daily_streak"] % 7,
                                multiplier=lang("multiplier_gift_bonus").format(
                                    multiplier=giver_profile["social"]["daily_streak"] // 7 + 1),
                                total_streak=giver_profile["social"]["daily_streak"]))
                else:
                    await ctx.send(lang("given").format(target=someone.mention, currency=self.bot.prefs.currency,
                                                        amount=amount, user=ctx.author.mention) + "\n")
        else:
            await ctx.send(lang("cooldown").format(hours=23 - time.gmtime().tm_hour, minutes=60 - time.gmtime().tm_min))

    # @commands.command(name="level")
    # async def level(self, ctx, who: MemberConverter=None):
    #     if who is None:
    #         who = ctx.author
    #     profile = profiles.safe_load(who)
    #     await ctx.send(self.lang("cmds.level.desc_formatting"))

    # Cancelled to increase the usage of profile checks (and interactions in case users notice something in the profile)


def setup(bot):
    bot.add_cog(SocialCog(bot))
