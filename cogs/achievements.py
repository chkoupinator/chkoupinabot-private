from lang.lang_wrap import LangWrap
import time
import sys
from cogs import profiles
from utils.exceptions import AchievementNotFound
from chkoupinabot import ChkoupinaBot

lang_settings = {
    "earned": "earned achievements",
    "upcoming": "upcoming achievements",
    "achievements": {
        "legacy_member": {
            "lvl1": {
                "name": "early adopter",
                "description": "has been in the server before this bot was even created! woop woop!"
            },
            "lvl2": {
                "name": "pre historic",
                "description": "has been quite active in the server before this bot was even created! amazing right?!"
            },
            "lvl3": {
                "name": "original gangster",
                "description": "was extremely active in this server before this bot was even created! "
                               "seriously they rock!"
            }
        },
        "profile_completed": {
            "name": "profile completed",
            "description": "has completed their profile with all the possible info!"
        },
        "profile_modified": {
            "name": "profile modified",
            "description": "has set one of the fields of their profile!"
        },
        "karma_given": {
            "lvl0": {
                "name": "karma given level 0",
                "description": "gave away their first karma point"
            }
        },
        "currency_earned": {
            "lvl1": {
                "name": "not a beggar",
                "description": "got a little bit of currency"
            },
            "lvl2": {
                "name": "hard worker",
                "description": "has made a bit more than a little bit of currency"
            }
        },
        "commands_used": {
            "lvl0": {
                "name": "commands used level 0",
                "description": "used their first command"
            }
        },
        "july_2018": {
            "lvl0": {
                "name": "june 2018 spectator",
                "description": "was here in june 2018!"
            },
            "lvl1": {
                "name": "june 2018 challenger",
                "description": "achieved level 10 in june 2018!"
            },
            "lvl2": {
                "name": "june 2018 champion",
                "description": "achieved level 25 in june 2018!"
            },
            "lvl3": {
                "name": "june 2018 legend",
                "description": "achieved level 50 in june 2018!"
            }
        },
        "jul-sep_2018": {
            "lvl0": {
                "name": "july-september 2018 spectator",
                "description": "was here in the 3rd season of  2018!"
            },
            "lvl1": {
                "name": "july-september 2018 challenger",
                "description": "achieved level 10 in the 3rd season of  2018!"
            },
            "lvl2": {
                "name": "july-september 2018 champion",
                "description": "achieved level 25 in the 3rd season of  2018!"
            },
            "lvl3": {
                "name": "july-september 2018 legend",
                "description": "achieved level 50 in the 3rd season of  2018!"
            }
        },
        "oct-dec_2018": {
            "lvl0": {
                "name": "october-december 2018 spectator",
                "description": "was here in the 4th season of 2018!"
            },
            "lvl1": {
                "name": "october-december 2018 challenger",
                "description": "achieved level 25 in the 4th season of 2018!"
            },
            "lvl2": {
                "name": "october-december 2018 champion",
                "description": "achieved level 50 in the 4th season of 2018!"
            },
            "lvl3": {
                "name": "october-december 2018 legend",
                "description": "achieved level 100 in the 4th season of 2018!"
            }
        },
        "oct-dec_2018_first": {
            "lvl0": {
                "name": "first to get october-december 2018 spectator",
                "description": "was the first to get level 1 in the 4th season of 2018!"
            },
            "lvl1": {
                "name": "october-december 2018 challenger",
                "description": "was the first to get level level 25 in the 4th season of 2018!"
            },
            "lvl2": {
                "name": "october-december 2018 champion",
                "description": "was the first to get level level 50 in the 4th season of 2018!"
            },
            "lvl3": {
                "name": "october-december 2018 legend",
                "description": "was the first to get level level 100 in the 4th season of 2018!"
            }
        },
        "jan-mar_2019": {
            "lvl0": {
                "name": "january-march 2019 spectator",
                "description": "was here in the 1st season of 2019!"
            },
            "lvl1": {
                "name": "january-march 2019 challenger",
                "description": "achieved level 25 in the 1st season of 2019!"
            },
            "lvl2": {
                "name": "january-march 2019 champion",
                "description": "achieved level 50 in the 1st season of 2019!"
            },
            "lvl3": {
                "name": "january-march 2019 legend",
                "description": "achieved level 100 in the 1st season of 2019!"
            }
        },
        "jan-mar_2019_first": {
            "lvl0": {
                "name": "first january-march 2019 spectator",
                "description": "was the first to get level 1 in the 1st season of 2019!"
            },
            "lvl1": {
                "name": "first january-march 2019 challenger",
                "description": "was the first to get level level 25 in the 1st season of 2019!"
            },
            "lvl2": {
                "name": "first january-march 2019 champion",
                "description": "was the first to get level level 50 in the 1st season of 2019!"
            },
            "lvl3": {
                "name": "first january-march 2019 legend",
                "description": "was the first to get level level 100 in the 1st season of 2019!"
            }
        },
        "apr-jun_2019": {
            "lvl0": {
                "name": "april-june 2019 spectator",
                "description": "was here in the 1st season of 2019!"
            },
            "lvl1": {
                "name": "april-june 2019 challenger",
                "description": "achieved level 25 in the 1st season of 2019!"
            },
            "lvl2": {
                "name": "april-june 2019 champion",
                "description": "achieved level 50 in the 1st season of 2019!"
            },
            "lvl3": {
                "name": "april-june 2019 legend",
                "description": "achieved level 100 in the 1st season of 2019!"
            }
        },
        "apr-jun_2019_first": {
            "lvl0": {
                "name": "first april-june 2019 spectator",
                "description": "was the first to get level 1 in the 1st season of 2019!"
            },
            "lvl1": {
                "name": "first april-june 2019 challenger",
                "description": "was the first to get level level 25 in the 1st season of 2019!"
            },
            "lvl2": {
                "name": "first april-june 2019 champion",
                "description": "was the first to get level level 50 in the 1st season of 2019!"
            },
            "lvl3": {
                "name": "first april-june 2019 legend",
                "description": "was the first to get level level 100 in the 1st season of 2019!"
            }
        }
    }
}


def unlocked(name, level=None):
    if name not in lang_settings["achievements"].keys():
        print(f"Error: Achievement named {name} not found!", file=sys.stderr)
        raise AchievementNotFound
    return {"name": name, "level": level, "time": time.time()}


class AchievementsCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        bot.achievements = self

    def short_lister(self, achievements):
        m = str()
        for a in achievements:
            unpacked = self.unpacker(a)
            m += f'**- {unpacked["title"]}:** {unpacked["description"]}.\n'
        return m

    def unpacker(self, achievement):
        if achievement["name"] not in lang_settings["achievements"].keys():
            print(f"Error: Achievement named {achievement['name']} not found!", file=sys.stderr)
            raise AchievementNotFound
        if achievement["level"] is not None:
            unpacked = {"title": self.lang(f"achievements.{achievement['name']}.lvl{achievement['level']}.name")}
        else:
            unpacked = {"title": self.lang(f"achievements.{achievement['name']}.name")}
        unpacked["description"] = self.lang(f"achievements.{achievement['name']}.description")
        return unpacked

    async def on_command(self, ctx):
        profile = profiles.safe_load(ctx.author)
        profile["tracking"]["commands_used"] += 1
        profiles.save(profile)


def setup(bot):
    bot.add_cog(AchievementsCog(bot))
