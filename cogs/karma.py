from lang.lang_wrap import LangWrap
from discord.ext import commands
import discord
from cogs import profiles
import traceback
import sys
from utils.converters import MemberConverter
from chkoupinabot import ChkoupinaBot


lang_settings = {
    "cmds": {
        "rep": {
            "title": "karma profile for {user.display_name}:",
            "description": "{who} can give {amount}/{max} more karma today",
            "received": "received",
            "questions": "questions",
            "trade": "trade",
            "lfg": "stw lfg",
            "br": "battleroyale",
            "total": "total",
            "given": "given",
            "given_total": "total",
            "given_today": "today",
            "target_not_found": "error: user named `{name}` not found, did you mean `{prefix}giverep <user> <karma "
                                "type> [amount]` instead?",
            "help": {
                "name": "rep",
                "usage": "`{prefix}rep [user]`\n"
                         "`[user]` can be a nickname, a username#disc, an @mention or a user id\n"
                         "shows the karma of the [user], or yours if no user is given",
                "short_desc": "shows the karma profile of users",
                "long_desc": "the command used to show your karma or look at others'"
            }
        },
        "giverep": {
            "error_self_or_bot": "error: you cannot give rep to a bot or yourself",
            "error_no_target": "error: You need to specify a user to give rep to, do `{prefix}help giverep` for more "
                               "info.",
            "target_not_found": "error: user named `{name}` not found",
            "error_on_type": "error, you didn't select a correct karma type to give, examples of correct "
                             "types are:\n{correct_types}",
            "error_no_type": "error: A type of karma to give is required for this command to work, "
                             "the karma types are:\n{correct_types}\nuse `{prefix}help giverep` for more info",

            "correct_types": "- `questions` or `help` for overall questions answering and helpfulness."
                             "\n- `trade` for trade reputation."
                             "\n- `\"save the world\"`, `lfg`, `pve` or `stw` for Save the World in game helpfulness "
                             "and good team play."
                             "\n- `battleroyale`, `br`, `\"team up\"` or `br`for BattleRoyale good team play/good "
                             "skills.",
            "error_cooldown": "you cannot give any more karma today (unless you level up)",
            "error_giving_too_much": "you do not have that amount of karma left to give, you can give {} "
                                     "more karma today (unless you level up)",
            "error_negative": "you cannot give negative karma (for now)",
            "error_not_int": "error: the last argument has to be a number",
            "types_revert_dict": {
                "questions": ["questions", "help"],
                "trade": ["trade"],
                "lfg": ["save the world", "lfg", "pve", "stw"],
                "br": ["battleroyale", "pvp", "team up", "br"],
            },
            "success": "you have successfully given {amount} {type} karma to {user.mention}, you can give {left} more "
                       "karma today (**{giver.name}**)",
            "help": {
                "name": "giverep",
                "usage": "`{prefix}giverep <user> <karma type> [amount]`\n`<user>` has to be a nickname, a "
                         "username#disc, an @mention or a user id (sorted by most to least accurate).\n"
                         "`<karma type>` has to be one of the following:\n"
                         "- `questions` or `help` for overall questions answering and helpfulness.\n"
                         "- `trade` for trade reputation.\n"
                         "- `\"save the world\"`, `lfg`, `pve` or `stw` for Save the World in game helpfulness and good"
                         " team play.\n"
                         "- `battleroyale`, `br`, `\"team up\"` or `br`for BattleRoyale good team play/good skills.\n"
                         "`[amount]` can be a number to give more than 1 rep at once.\n"
                         "Gives `[amount]` (or 1 if unspecified) of `<karma type>` to a `<user>`.",
                "short_desc": "used to give karma/rep to others",
                "long_desc": "Used to give karma/reputation of a certain karma type (listed below) to a user."
            }
        }

    },
    "help": {
        "name": "karma module",
        "short_desc": "the module containing everything related to karma and reputation",
        "long_desc": "the module containing the karma and reputation related commands, also responsible of "
                     "things such as karma leaderboards and karma exchange, requires the profile module"
    }
}

karma_types = ["questions", "trade", "lfg", "br", "total"]


def can_be_given_daily(profile):
    return ((profile["social"]["month_level"] // 10) + 1) * 4


def karma_left_to_give(profile):
    return can_be_given_daily(profile) - profile["karma"]["given_today"]


class KarmaCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)

    @commands.command(name="rep", aliases=["karma", "reputation"])
    async def rep(self, ctx, *, who: MemberConverter = None):
        if who is None:
            who = ctx.author
        profile = profiles.safe_load(who)
        embed = discord.Embed(
            title=self.lang("cmds.rep.title").format(user=who),
            description=self.lang("cmds.rep.description").format(who="You" if ctx.author == who else who.display_name,
                                                                 amount=karma_left_to_give(profile),
                                                                 max=can_be_given_daily(profile)))
        m = str()
        for karma_type in karma_types:
            m += self.lang(f"cmds.rep.{karma_type}") + f': {profile["karma"][karma_type]}\n'
        embed.add_field(name=self.lang("cmds.rep.received")+":", value=m)

        m = str()
        for karma_type in ["given_today", "given_total"]:
            m += self.lang(f"cmds.rep.{karma_type}") + f': {profile["karma"][karma_type]}\n'
        embed.add_field(name=self.lang("cmds.rep.given") + ":", value=m)

        # TODO add karma leaderboards
        # TODO add achievement potential unlock

        await ctx.send(embed=embed)

    @rep.error
    async def rep_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            if "Member" in str(error):
                return await ctx.send(self.lang("cmds.rep.target_not_found").format(
                    name=str(error).lstrip('Member "').rstrip('" not found'), prefix=self.prefix))
        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)

    @commands.command(name="giverep", aliases=["givekarma", "grep"])
    async def giverep(self, ctx, who: MemberConverter, ktype: str, *, amount: int = 1):
        if who.bot or who == ctx.author:
            return await ctx.send(self.lang("cmds.giverep.error_self_or_bot"))
        if amount < 0:
            return await ctx.send(self.lang("cmds.giverep.error_negative"))
        giver_profile = profiles.safe_load(ctx.author)
        left_to_give = karma_left_to_give(giver_profile)
        if not left_to_give:
            return await ctx.send(self.lang("cmds.giverep.error_cooldown"))
        if left_to_give - amount < 0:
            return await ctx.send(self.lang("cmds.giverep.error_giving_too_much").format(left_to_give))
        reversed_dict = dict()
        types_dict = lang_settings["cmds"]["giverep"]["types_revert_dict"]
        for key in types_dict.keys():
            for value in types_dict[key]:
                reversed_dict[value] = key
        ktype = ktype.lower()
        if ktype not in reversed_dict.keys():
            return await ctx.send(self.lang("cmds.giverep.error_on_type").format(
                correct_types=self.lang("cmds.giverep.correct_types")))
        target_profile = profiles.safe_load(who)
        target_profile["karma"][reversed_dict[ktype]] += amount
        target_profile["karma"]["total"] += amount
        giver_profile["karma"]["given_today"] += amount
        giver_profile["karma"]["given_total"] += amount
        profiles.save(target_profile)
        profiles.save(giver_profile)
        await ctx.send(self.lang("cmds.giverep.success").format(amount=amount, type=reversed_dict[ktype], user=who,
                                                                left=left_to_give - amount, giver=ctx.author))

    @giverep.error
    async def giverep_error(self, ctx, error):
        if isinstance(error, commands.MissingRequiredArgument):
            if "ktype" in str(error):
                return await ctx.send(self.lang("cmds.giverep.error_no_type").format(
                    correct_types=self.lang("cmds.giverep.correct_types"), prefix=self.prefix))
            if "who" in str(error):
                return await ctx.send(self.lang("cmds.giverep.error_no_target").format(prefix=self.prefix))
        if isinstance(error, commands.BadArgument):
            if "Member" in str(error):
                return await ctx.send(self.lang("cmds.giverep.target_not_found").format(
                    name=str(error).lstrip('Member "').rstrip('" not found')))
            elif "Converting to \"int\" failed" in str(error):
                return await ctx.send(self.lang("cmds.giverep.error_not_int"))
        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)


def setup(bot):
    bot.add_cog(KarmaCog(bot))
