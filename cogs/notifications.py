import sys
import traceback

from discord.ext import commands
import discord
from lang.lang_wrap import LangWrap
import asyncio
from chkoupinabot import ChkoupinaBot
import re

lang_settings = {
    "cmds": {
        "notify": {
            "help": {
                "name": "notify",
                "usage": "`{prefix}notify <channel> <role> [-a] [message to send]`"
                         "\n`<channel>` can be a channel mention, a channel id or a channel name."
                         "\n`<role>` can be a role mention, a role id or a role name (use \"quotes for names with "
                         "spaces\")\nsends the [message to send] in the `<channel>` and starts it by @mentioning the "
                         "specified `<role>`, sends only an @mention if no message is specified\nif the message "
                         "contains an @mention of the `<role>` then no @mention is added at the beginning\ndoes not add"
                         " the name of the sender if used with the `-a` flag",
                "short_desc": "notifies a mentionable role in a channel with a given message (staff only)",
                "long_desc": "a command that can be used by the staff members that have the notificator role which "
                             "makes a role mentionable for a short period of time to notify the users that have it of "
                             "something, in a given channel"
            },
            "success": "the message has successfully been sent in <#{channel_id}> and mentioned the role "
                       "<@&{role_id}>",
            "not_notifiable": "the role <@&{role_id}> isn't a notifiable role",
            "sent_by": "\nsent by {who}"
        }
    },
    "alerts": {
        "vbucks_single_message": "M-m-m-moneyshot <:vbucks:441539197082533889> <@&{role_id}>",
        "total_message": "**{total}<:vbucks:441539197082533889> total, region breakdown:**\n{regions}",
        "region_format": "{sub_total}<:vbucks:441539197082533889> in {region}",
        "legendary": "shiny! a legendary mission appeared! <@&{role_id}>",
        "daily_llama": "new daily llama! <@&{role_id}>"
    },
    "help": {
        "name": "notifications module",
        "short_desc": "takes care of notifications, role mentions and the such",
        "long_desc": "the module that takes care of carefully mentioning roles to avoid abuse, also scraps some data "
                     "from stormshield one"
    }
}

VBUCKS_WAIT = 300


class NotificationsCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        self.summing_initiated = False
        self.vbucks = {
            "Stonewood": {
                "sum": 0,
                "embeds": []
            },
            "Plankerton": {
                "sum": 0,
                "embeds": []
            },
            "Canny Valley": {
                "sum": 0,
                "embeds": []
            },
            "Twine Peaks": {
                "sum": 0,
                "embeds": []
            },
            "Unknown": {
                "sum": 0,
                "embeds": []
            }}
        self.listen_channel_id = 351762740827914262
        self.timed_channel_id = 461716263039795210
        self.timed_channel = None  # type: discord.TextChannel
        self.llama_channel_id = 461716429440417823
        self.llama_channel = None  # type: discord.TextChannel
        self.vbucks_regex = re.compile(r'\(x(\d+)\)')
        self.vbucks_role_id = 460257663813681154
        self.vbucks_role = None  # type: discord.TextChannel
        self.legendary_role_id = 344429301921349643
        self.legendary_role = None  # type: discord.TextChannel
        self.legendary_mentions_open = False
        self.mythic_role_id = 344429316022599680
        self.mythic_role = None  # type: discord.TextChannel
        self.llama_role_id = 344429448789098498
        self.llama_role = None  # type: discord.TextChannel

        # self.server_news_channel_id = 326503262343790593  # currently discord moderation
        # self.server_news_channel = None
        # self.server_news_role_id = 460263270406684677
        # self.server_news_role = None
        #
        # self.stw_news_channel_id = 449480741680709652  # currently chkoupinabot testing
        # self.stw_news_channel = None
        # self.stw_news_role_id = 460257664933429259
        # self.stw_news_role = None
        #
        # self.br_news_channel_id = 332300593500651522  # currently mod botspam
        # self.br_news_channel = None
        # self.br_news_role_id = 460257664472055809
        # self.br_news_role = None

        self.notifiable_roles = [
            460263270406684677,  # Server News
            460257664933429259,  # StW News
            460257664472055809,  # BR News
            460257663813681154,  # V-Bucks Alert
            344429301921349643,  # Legendary Alert
            344429316022599680,  # Mythic Alert
            344429448789098498,  # Daily Llama
            460263274118905856,  # Free Games
            460263273237839884,  # Giveaways
            460265418230202388,  # Contests
            484518187715788812  # Chkoupinafan
        ]

    def reset_vbuck_missions(self):
        self.summing_initiated = False
        self.vbucks = {
            "Stonewood": {
                "sum": 0,
                "embeds": []
            },
            "Plankerton": {
                "sum": 0,
                "embeds": []
            },
            "Canny Valley": {
                "sum": 0,
                "embeds": []
            },
            "Twine Peaks": {
                "sum": 0,
                "embeds": []
            },
            "Unknown": {
                "sum": 0,
                "embeds": []
            }}

    async def on_message(self, msg):
        if msg.channel.id == self.listen_channel_id and msg.embeds:
            if not self.vbucks_role:
                self.vbucks_role = discord.utils.get(msg.guild.roles, id=self.vbucks_role_id)
            if not self.legendary_role:
                self.legendary_role = discord.utils.get(msg.guild.roles, id=self.legendary_role_id)
            if not self.mythic_role:
                self.mythic_role = discord.utils.get(msg.guild.roles, id=self.mythic_role_id)
            if not self.timed_channel:
                self.timed_channel = msg.guild.get_channel(self.timed_channel_id)
            if not self.llama_channel:
                self.llama_channel = msg.guild.get_channel(self.llama_channel_id)
            if not self.llama_role:
                self.llama_role = discord.utils.get(msg.guild.roles, id=self.llama_role_id)

            embed = msg.embeds[0]
            if embed.title == "Mission Alert":
                if "vBucks" in embed.description:
                    try:
                        amount = int(self.vbucks_regex.search(embed.description).group(1))
                    except ValueError:
                        amount = None

                    def region_sum(region):
                        if amount is None:
                            self.vbucks[region]["sum"] = None
                        elif self.vbucks[region]["sum"] is not None:
                            self.vbucks[region]["sum"] += amount
                        self.vbucks[region]["embeds"].append(embed)

                    if "Twine Peaks" in embed.description:
                        region_sum("Twine Peaks")
                    elif "Canny Valley" in embed.description:
                        region_sum("Canny Valley")
                    elif "Plankerton" in embed.description:
                        region_sum("Plankerton")
                    elif "Stonewood" in embed.description:
                        region_sum("Stonewood")
                    else:
                        region_sum("Unknown")
                    if self.summing_initiated:
                        return
                    self.summing_initiated = True
                    await asyncio.sleep(VBUCKS_WAIT)
                    total_sums = []
                    total_sum = 0
                    await self.vbucks_role.edit(mentionable=True)
                    for region in self.vbucks.keys():
                        if self.vbucks[region]["sum"] is None:
                            for embed in self.vbucks[region]["embeds"]:
                                await self.timed_channel.send(self.lang("alerts.vbucks_single_message").format(
                                    role_id=self.vbucks_role_id), embed=embed)
                            total_sums.append([region, "Unknown"])
                        elif self.vbucks[region]["sum"] > 0:
                            for embed in self.vbucks[region]["embeds"]:
                                await self.timed_channel.send(self.lang("alerts.vbucks_single_message").format(
                                    role_id=self.vbucks_role_id), embed=embed)
                            total_sums.append([region, self.vbucks[region]["sum"]])
                            total_sum += self.vbucks[region]["sum"]
                    await self.timed_channel.send(self.lang("alerts.total_message").format(
                        total=total_sum, regions="\n".join(
                            self.lang("alerts.region_format").format(region=l[0], sub_total=l[1]) for l in total_sums)))
                    self.reset_vbuck_missions()
                    await self.vbucks_role.edit(mentionable=False)
                else:
                    if not self.legendary_mentions_open:
                        await self.legendary_role.edit(mentionable=True)
                        self.legendary_mentions_open = True
                        await self.timed_channel.send(self.lang("alerts.legendary").format(
                            role_id=self.legendary_role_id), embed=embed)
                        await asyncio.sleep(30)
                        await self.legendary_role.edit(mentionable=False)
                        self.legendary_mentions_open = False
                    else:
                        await self.timed_channel.send(self.lang("alerts.legendary").format(
                            role_id=self.legendary_role_id), embed=embed)
            elif embed.title == "Daily Llama":
                await self.llama_role.edit(mentionable=True)
                await self.llama_channel.send(self.lang("alerts.daily_llama").format(
                    role_id=self.llama_role_id), embed=embed)
                await self.llama_role.edit(mentionable=False)

    @commands.command(name="notify")
    @commands.has_any_role("Admin", "Notificator")
    async def notify(self, ctx: commands.Context, channel: discord.TextChannel, role: discord.Role, *, message: str=""):
        anon = False
        shadowping = (len(message) == 0)
        if message.startswith('-a '):
            message = message.lstrip('-a ')
            anon = True

        if role.id not in self.notifiable_roles:
            return await ctx.send(self.lang("cmds.notify.not_notifiable").format(role_id=role.id))

        await role.edit(mentionable=True, reason=f"{ctx.author} used notify command in #{channel}")

        if not anon and not shadowping:
            message = f'{message}{self.lang("cmds.notify.sent_by").format(who=ctx.author.mention)}'

        if role.mention not in message:
            message = f'{role.mention} {message}'

        message_sent = await channel.send(message)  # type: discord.Message

        if shadowping:
            await message_sent.delete()

        # TODO make it send an embed and add an image optional field
        await role.edit(mentionable=False, reason=f"{ctx.author} used notify command in #{channel}")
        await ctx.send(f'{self.lang("cmds.notify.success").format(role_id=role.id, channel_id=channel.id)}'
                       f'{" And got removed successfully for maximum shadowpinging" if shadowping else ""}')

    @notify.error
    async def notify_error(self, ctx, error):
        if isinstance(error, commands.BadArgument):
            return await ctx.send(f'**BadArgument Error:** {error}')
        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)


def setup(bot):
    bot.add_cog(NotificationsCog(bot))
