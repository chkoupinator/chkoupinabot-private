from cogs import help, utility, profiles, social, karma, achievements, roles, notifications, economy, moderation

lang_map = {
    "HelpCog": help.lang_settings,
    "UtilityCog": utility.lang_settings,
    "ProfilesCog": profiles.lang_settings,
    "SocialCog": social.lang_settings,
    "KarmaCog": karma.lang_settings,
    "RolesCog": roles.lang_settings,
    "NotificationsCog": notifications.lang_settings,
    "EconomyCog": economy.lang_settings,
    "ModerationCog": moderation.lang_settings
}
