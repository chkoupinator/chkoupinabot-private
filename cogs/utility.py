import discord
from discord.ext import commands
import time
import asyncio
from utils.misc import icmp_ping
from lang.lang_wrap import LangWrap
from utils import persistence
from chkoupinabot import ChkoupinaBot
import json

lang_settings = {
    "cmds": {
        "reload": {
            "lang_reloaded": "lang successfully reloaded",
            "lang_warnings": "however several warnings have been returned, which are:",
            "prefs_success": "prefs successfully reloaded",
            "help": {
                "name": "reload",
                "usage": "`{prefix}reload [what to reload]`\n"
                         "reloads what you want to reload, [what to reload] can currently only be `lang` or `language`",
                "short_desc": "utility function for bot owners to reload some parts of the bot",
                "long_desc": "utility function for bot owners to reload some parts of the bot, like the language "
                             "strings of the bot for instance"
            }
        },
        "ping": {
            "chkoupong": "chkoupong! heartbeat {hb} icmp {icmp}",
            "pong": "pong! heartbeat {hb} icmp {icmp}",
            "help": {
                "name": "ping",
                "usage": "`{prefix}ping [number of times]`\n"
                         "gets the ping to discord [number of times], 1 if not provided ",
                "short_desc": "small utility function to check the ping and latency to discord",
                "long_desc": "a small utility function that triggers typing to calculate the heartbeat of the bot, and "
                             "also sends an icmp ping to discordapp.com to check the latency to discord itself"
            }
        },
        "botname": {
            "sucess": "name has been successfully set to {name}",
            "help": {
                "name": "botname",
                "usage": "`{prefix}botname <new name>`\n"
                         "changes the name of the bot to [new name]",
                "short_desc": "small utility function for the bot owner to change the name of the bot",
                "long_desc": "a small utility function for the bot owner that changes the name of the bot as perceived "
                             "everywhere (not the nickname) to the name given to it"
            }
        },
        "request": {
            "help": {
                "name": "request",
                "usage": "`{prefix}request <your request>`\n"
                         "sends a message to Chkoupinator with your request",
                "short_desc": "used to send requests to Chkoupinator",
                "long_desc": "a command that allows you to send requests to Chkoupinator"
            },
            "format": "request by {u} userid {u.id}:\n{message}",
            "no_message": "please specify a request to send to Chkoupinator",
            "success": "message successfully sent to Chkoupinator"
        },
        "bounties": {
            "help": {
                "name": "bounties",
                "usage": "`{prefix}bounties [[-s <sort by> [-o <order>]] | [name of feature]]`\n"
                         "`-s <sort_by>` sorts the list of all the bounties by the `<sort by>`, can be either: `name`, "
                         "`goals_reached`, `tips`, `votes` or `in_progress`\n"
                         "`-o <order>` determines the order in which it is sorted, can be either `up`/`ascending` "
                         "to have the values sorted from smaller to bigger or `down`/`descending` to have the values "
                         "sorted from the biggest to the smallest.\n"
                         "`name of feature` can be the name of the feature you want to look at.\n"
                         "Examples:\n"
                         "`{prefix}bounties -s votes -o down`\n"
                         "`{prefix}bounties -s tips`\n"
                         "`{prefix}bounty karma rework`",
                "short_desc": "shows upcoming features and the votes and tips towards their completion",
                "long_desc": "Lets you look at all the upcoming features for Chkoupinabot with many sorting options, "
                             "or, can be used to look at the details of a specific feature"

            },
            "title": "bounties/votes for Chkoupinabot features",
            "message": "list of the current bounties for the upcoming Chkoupinabot features (you can use "
                       "`{prefix}request` to request for a new one to be added) sorted by {sorted_by} ({order}):\n"
                       "**<Name>**, votes: `<votes>`, tips: `<amount>`, sub-goals: `<reached>/<total>`\n",
            "invalid_sorting": "the sorting condition you specified isn't a valid sorting argument, please do "
                               "`{prefix}help bounties` for more info about the valid sorting methods",
            "invalid_order": "the order you specified has not been recognized  please do "
                               "`{prefix}help bounties` for more info about the valid order keywords",
            "not_found": "the bounty called `{name}` has not been found"
        }
    },
    "misc": {
        "ping_failed": "failed"
    },
    "help": {
        "name": "utility module",
        "long_desc": "utility module, has tools that help test the ping and other things of the bot",
        "short_desc": "utility module, contains useful commands to test the bot"
    }
}

false = False
empty_bounty = {
    "name": "",
    "aliases": [],
    "desc": "",
    "goals": {},
    "goals_reached": 0,
    "tips": 0,
    "votes": 0,
    "in_progress": false
}


def load_bounties():
    with open("features.json", 'r') as bounties_file:
        return json.load(bounties_file)


def sort_bounties(bounties, by: str, order: str):
    bounties_sorted = [bounties[0]]
    bounties = bounties[1:]
    if order == "up":
        for bounty in bounties:
            for i in range(0, len(bounties_sorted)):
                if bounty[by] <= bounties_sorted[i][by]:
                    bounties_sorted.insert(i, bounty)
                    break
            else:
                bounties_sorted.append(bounty)
    elif order == "down":
        for bounty in bounties:
            for i in range(0, len(bounties_sorted)):
                if bounty[by] >= bounties_sorted[i][by]:
                    bounties_sorted.insert(i, bounty)
                    break
            else:
                bounties_sorted.append(bounty)
    return bounties_sorted


class UtilityCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        self.bounties_loaded = load_bounties()  # type: list

        super().__init__(bot)

    @commands.command(name='ping', aliases=['chkouping'])
    async def ping(self, ctx, *, times: int = 1):
        repeat = 1
        if times > 0:
            repeat = times
        if repeat > 10:
            repeat = 10
        while repeat > 0:
            t1 = time.perf_counter()
            await ctx.trigger_typing()
            t2 = time.perf_counter()
            ping = icmp_ping("discordapp.com")
            if ping is None:
                ping = self.lang("misc.ping_failed")
            else:
                ping = f'{ping}ms'
            heartbeat = f'{round((t2-t1)*1000)}ms'
            if "chkou" in ctx.invoked_with:
                chkou = "chkou"
            else:
                chkou = ""

            await ctx.send(self.lang(f"cmds.ping.{chkou}pong").format(hb=heartbeat, icmp=ping))

            # if "chkou" in ctx.invoked_with:
            #     await ctx.send('Chkoupong!\nHeartbeat time: ' + str(round((t2 - t1) * 1000)) + "ms\n"
            #                    f"ICMP ping to `discordapp.com`: {ping}")
            # else:
            #     await ctx.send("Pong!\nHeartbeat time: " + str(round((t2 - t1) * 1000)) + "ms\n"
            #                    f"ICMP ping to `discordapp.com`: {ping}")
            repeat -= 1
            await asyncio.sleep(5)

    @commands.command(name='reload')
    @commands.is_owner()
    async def reload(self, ctx, *, what: str):
        if "lang" in what:
            print("Admin requested lang module reload")
            warnings = self.bot.lang.reload_lang()
            answer = self.lang("cmds.reload.lang_reloaded")
            if warnings:
                if len(f"{answer}, {self.lang('cmds.reload.lang_warnings')}:\n```diff{warnings}```") > 1999:
                    answer = f"{answer}, {self.lang('cmds.reload.lang_warnings')}:\n```diff\n"
                    for line in warnings.split("\n"):
                        if len(answer + line) > 1995:
                            await ctx.author.send(answer + "```")
                            answer = "```diff\n" + line
                        else:
                            answer = answer + "\n" + line
                    answer = answer + "```"
                else:
                    answer = f"{answer}, {self.lang('cmds.reload.lang_warnings')}:\n```diff{warnings}```"
            await ctx.author.send(answer)
        if "pref" in what:
            print("Admin requested prefs reload")
            self.bot.prefs.reload()
            await ctx.send("prefs reloaded")
        if "day" in what:
            print("Admin requested day restart")
            persistence.save("last_check", persistence.get("last_check") - 86400)
            await ctx.send("day restarted")

    @commands.command(name='botname')
    @commands.is_owner()
    async def botname(self, ctx, *, new_name: str):
        await ctx.bot.user.edit(username=new_name)
        await ctx.send(self.lang("cmds.botname.sucess").format(name=new_name))

    @commands.command(name='request')
    async def request(self, ctx: discord.ext.commands.Context, *, message: str=None):
        if message is None:
            return await ctx.send(self.lang("cmds.request.no_message"))
        if not self.bot.chkoupinator:
            self.bot.chkoupinator = discord.utils.get(self.bot.guilds, id=self.bot.prefs.main_guild).owner
        await self.bot.chkoupinator.send(self.lang("cmds.request.format").format(u=ctx.author, message=message))
        await ctx.send(self.lang("cmds.request.success"))

    def show_bounties(self, bounties, sorted_by="Date added", order="Ascending"):
        message = self.lang("cmds.bounties.message").format(sorted_by=sorted_by, order=order, prefix=self.prefix)
        for b in bounties:
            message = f"{message} - **{b['name']}**, votes: `{b['votes']}`, tips: `{b['tips']}`, " \
                      f"sub-goals: `{b['goals_reached']}/{len(b['goals'])}`\n"

        return discord.Embed(title=self.lang("cmds.bounties.title"), description=message)

    @commands.command(name='bounties', aliases=["bounty", "tips", "tip", "donate", "donations", "features", "vote",
                                                "feature"])
    async def bounties(self, ctx: discord.ext.commands.Context, *, bounty: str=str()):
        args = bounty.split(" ")
        bounty = str()
        i = 0
        sort = None
        order = None
        while i < len(args):
            if len(args[i]) == 0:
                i += 1
                continue
            if args[i].startswith("-s"):
                try:
                    sort = args[i+1].lower()
                except IndexError:
                    pass
                i += 2
            elif args[i].startswith("-o"):
                try:
                    sort = args[i+1].lower()
                except IndexError:
                    pass
                i += 2
            else:
                bounty = f"{bounty} {args[i]}"
                i += 1
        if sort is not None:
            if sort not in ["name", "goals_reached", "tips", "votes", "in_progress"]:
                return await ctx.send(self.lang("cmds.bounties.invalid_sorting").format(prefix=self.prefix))
        if order is not None:
            if not order.startswith("asc") and not order.startswith("des") and order not in ["up", "down"]:
                return await ctx.send(self.lang("cmds.bounties.invalid_order").format(prefix=self.prefix))
        if len(bounty) == 0:
            if self.bounties_loaded is None:
                self.bounties_loaded = load_bounties()
            if sort is None:
                if order is None:
                    return await ctx.send(embed=self.show_bounties(self.bounties_loaded))
            else:
                if order is None:
                    if sort == "name":
                        return await ctx.send(embed=self.show_bounties(sort_bounties(self.bounties_loaded,
                                                                                     sort, "up"), "Name"))
                    elif sort == "goals_reached":
                        return await ctx.send(embed=self.show_bounties(sort_bounties(self.bounties_loaded,
                                                                                     sort, "down"), "Goals Reached",
                                                                       "Descending"))
                    elif sort == "tips":
                        return await ctx.send(embed=self.show_bounties(sort_bounties(self.bounties_loaded,
                                                                                     sort, "down"), "Amount Donated",
                                                                       "Descending"))
                    elif sort == "votes":
                        return await ctx.send(embed=self.show_bounties(sort_bounties(self.bounties_loaded,
                                                                                     sort, "down"), "Votes",
                                                                       "Descending"))
                    elif sort == "in_progress":
                        return await ctx.send(embed=self.show_bounties(sort_bounties(self.bounties_loaded,
                                                                                     sort, "down"), "Being Worked on",
                                                                       "True before False"))
                elif order.startswith("des") or order == "down":
                    return await ctx.send(embed=self.show_bounties(sort_bounties(self.bounties_loaded,
                                                                                 sort, "down"), f'`{sort}`' 
                                                                                                "Descending"))
                elif order.startswith("asc") or order == "up":
                    return await ctx.send(embed=self.show_bounties(sort_bounties(self.bounties_loaded,
                                                                                 sort, "up"), f'`{sort}`', "Ascending"))
        else:
            bounty = bounty.lower().strip(" ").strip('"').strip(' ')
            if self.bounties_loaded is None:
                self.bounties_loaded = load_bounties()
            for b in self.bounties_loaded:
                if (b["name"].lower() == bounty) or (bounty in b["aliases"]):
                    embed = discord.Embed(title=b["name"], description=b["desc"])
                    embed.add_field(name="Goals",
                                    value="\n".join(f"{'+' if  b['tips']>int(v) else '-'}{v}€: {d}" for v, d in
                                                    b["goals"].items()))
                    embed.add_field(name="Status", value=f"Worked on: {b['in_progress']}, Votes: {b['votes']}, "
                                                         f"Donated: {b['tips']}, "
                                                         f"Goals reached: {b['goals_reached']}/{len(b['goals'])}")
                    return await ctx.send(embed=embed)
            else:
                return await ctx.send(self.lang("cmds.bounties.not_found").format(name=bounty))


def setup(bot):
    bot.add_cog(UtilityCog(bot))
