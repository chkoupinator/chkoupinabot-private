from discord.ext import commands
from lang.lang_wrap import LangWrap
import discord
import asyncio
from chkoupinabot import ChkoupinaBot

lang_settings = {
    "cmds": {
        "roles": {
            "embed_title": "list of self assignable roles",
            "embed_desc": "this is the list of the roles you can self assign with `{prefix}getrole <role name>`, note "
                          "that any alias (when applicable) and any upper/lower case combination works too:\n"
                          "**```diff\n- DO NOT @MENTION THE ROLE!!! -\nOnly take the name which comes after the @```**",
            "roles_description": {
                "Save the World": "gives access the save the world voice and text channels",
                "BattleRoyale": "gives access the battleroyale voice and text channels",
                "Server News": "a notification role that will get @mentioned on relevant server related news",
                "StW News": "a role that will get @mentioned on relevant save the world related news",
                "BR News": "a role that will get @mentioned on relevant battleroyale related news",
                "V-Bucks Alert": "a notification role for timed missions with v-bucks as a reward "
                                 "<:vbucks:441539197082533889>",
                "Legendary Alert": "a notification role for timed missions with legendary rewards",
                "Mythic Alert": "a notification role for timed missions with mythic rewards",
                "Daily Llama": "a notification role for the daily llama rotation message",
                "Free Games": "a notification role for when (somewhat worth anything) games become free to grab",
                "Giveaways": "a notification role for when there is a giveaway going on",
                "Contests": "a notification role for when there is a contest going on",
                "SSD Specialist": "a role for people that want to help with StormShield Defenses",
                "Llama Specialist": "a role for people that want to answer the questions of people of people faced with"
                                    " a choice while opening a llama",
                "Missions Saviour": "a role for people that want to help with missions that got started and invested in"
                                    " (defenses built etc) and no help could be found while it is needed",
                "Questions Helper": "a role for people that want to answer questions that don't get answered directly "
                                    "in <#337381568060588052>",
                "Chkoupinabot Fan/Tester": "a role for people that want to help testing Chkoupinabot or that want to "
                                           "show their support for the bot (not mentionable)",
            },
            "role_format": "**{role}** (alias: `{aliases}`):\n"
                           "  {description}\n",
            "categories": {
                "access_roles": {
                    "name": "access roles",
                    "description": "roles that are used with the discord permissions to give you access to different "
                                   "parts of the server"
                },
                "news_roles": {
                    "name": "news roles",
                    "description": "notification roles that will get @mentioned whenever there are relevant news"
                },
                "timed_missions": {
                    "name": "stw timed mission roles",
                    "description": "notification roles that will get @mentioned on the appearance of timed missions"
                },
                "notification_roles": {
                    "name": "notification roles",
                    "description": "notification roles that will get @mentioned on the occasions corresponding to them"
                },
                "helper_roles": {
                    "name": "helper roles",
                    "description": "roles that can get @mentioned by anyone in need of help"
                },
            },
            "help": {
                "name": "roles",
                "usage": "`{prefix}roles`\nlists the self assignable roles, do `{prefix}help getrole` to look how to "
                         "get them",
                "short_desc": "lists the self assignable roles",
                "long_desc": "lists the roles that can be self assigned by using the `getrole` command and removed with"
                             " the `delrole` command"
            }
        },
        "getrole": {
            "help": {
                "name": "getrole",
                "usage": "`{prefix}getrole <role name>`\n`<role name>` has to be one of the self assignable roles, use "
                         "`{prefix}roles` to show them\ngives you the role corresponding to a given `<role name>`\n"
                         "example: `{prefix}getrole member`",
                "short_desc": "used to get a self assignable role",
                "long_desc": "used to give you one of the self assignable roles that are listed with the `roles` "
                             "command, use `delrole` to remove a role that you got with this command"
            },
            "admin": "you are now **an admin**!!1!1!1!!",
            "owner": "you are now **the owner**!!1!1!1!1!",
            "incorrect_role": "error: the name of the role you gave is not recognized, please do `{prefix}roles` to "
                              "see the list of the self assignable roles",
            "success": "role **`{role}`** successfully equipped <:pepe_ok_hand:382198933134245888>",
            "already_equipped": "role already equipped, if you want to remove it use `{prefix}delrole` instead",
            "required_not_equipped": "error: you need to get the **`{required}`** role before getting that role",
            "missing_member_role": "error: you need to read the rules in <#275321943085809674> and accept them by "
                                   "getting the member role before being able to get the rest of the roles"
        },
        "delrole": {
            "help": {
                "name": "delrole",
                "usage": "`{prefix}delrole <role name>`\n`<role name` has to be the name of a role you have currently "
                         "equipped, you can see the correspondence between roles and names using `{prefix}roles` but "
                         "typing the name of the role as you see it when you click on yourself should work too.`\n"
                         "removes the role corresponding to `<role name>` from the list of your current roles\n"
                         "example: `{prefix}delrole mythic` or `{prefix}delrole mythic timed` both are working ways to "
                         "remove the mythic timed role",
                "short_desc": "used to unequip a self assignable role",
                "long_desc": "used to remove a self assignable role that you do not want anymore"
            },
            "no_args": "this should probably show you the list of the roles you have equipped but I'm lazy and I have "
                       "other things to do right now so please just click on your profile picture (or long press it "
                       "on phone) to see your roles and do `{prefix}delrole <name of the role you want removed>` do "
                       "`{prefix}help delrole` for more info",
            "success": "role **`{role}`** successfully removed <:pepe_ok_hand:382198933134245888>",
            "role_not_equipped": "error: you do not have the role **`{role}`**, if you want to get it use "
                                 "`{prefix}getrole {role}` instead"
        },
        "member": {
            "help": {
                "name": "member",
                "usage": "`{prefix}member` gives you the member role",
                "short_desc": "for users who have read the rules",
                "long_desc": "use this command if you have read the rules to get the member role, using it without "
                             "reading the rules would still mean that you accept them"
            },
            "success": "**{username}** accepted the rules and has successfully been given the <@&{roleid}> role!",
            "already_member": "<@{userid}> you are already a member!"
        }
    },
    "help": {
        "name": "roles module",
        "short_desc": "the module that takes care of roles",
        "long_desc": "the module responsible for giving, listing **and removing** roles"
    }
}


class RolesCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        self.member_role_id = 327185317167759360
        self.self_roles = {
            "access_roles": {
                "Save the World": 460267845826117633,
                "BattleRoyale": 460267830848258059
            },
            "news_roles": {
                "Server News": 460263270406684677,
                "StW News": 460257664933429259,
                "BR News": 460257664472055809
            },
            "timed_missions": {
                "V-Bucks Alert": 460257663813681154,
                "Mythic Alert": 344429316022599680,
                "Legendary Alert": 344429301921349643
            },
            "notification_roles": {
                "Daily Llama": 344429448789098498,
                "Free Games": 460263274118905856,
                "Giveaways": 460263273237839884,
                "Contests": 460265418230202388
            },
            "helper_roles": {
                "SSD Specialist": 460866139015348234,
                "Llama Specialist": 460866139627716608,
                "Missions Saviour": 460866141049454602,
                "Questions Helper": 460866140311257099,
                "Chkoupinabot Fan/Tester": 484518187715788812
            }
        }
        self.self_roles_ids_dict = {
            "Save the World": 460267845826117633,
            "BattleRoyale": 460267830848258059,
            "Server News": 460263270406684677,
            "StW News": 460257664933429259,
            "BR News": 460257664472055809,
            "V-Bucks Alert": {
                "id": 460257663813681154,
                "required": "Save the World"
            },
            "Legendary Alert": {
                "id": 344429301921349643,
                "required": "Save the World"
            },
            "Mythic Alert": {
                "id": 344429316022599680,
                "required": "Save the World"
            },
            "Daily Llama": 344429448789098498,
            "Free Games": 460263274118905856,
            "Giveaways": 460263273237839884,
            "Contests": 460265418230202388,
            "SSD Specialist": {
                "id": 460866139015348234,
                "required": "Save the World"
            },
            "Llama Specialist": {
                "id": 460866139627716608,
                "required": "Save the World"
            },
            "Missions Saviour": {
                "id": 460866141049454602,
                "required": "Save the World"
            },
            "Questions Helper": 460866140311257099,
            "Chkoupinabot Fan/Tester": 484518187715788812,
            "Cuddly": 338224459762761730
        }

        self.self_role_aliases = {
            "pve": "Save the World",
            "stw": "Save the World",
            "save the world": "Save the World",
            "pvp": "BattleRoyale",
            "br": "BattleRoyale",
            "battleroyale": "BattleRoyale",
            "battle royale": "BattleRoyale",
            "news": "Server News",
            "server news": "Server News",
            "stw news": "StW News",
            "pve news": "StW News",
            "br news": "BR News",
            "pvp news": "BR News",
            "vbucks": "V-Bucks Alert",
            "v-bucks": "V-Bucks Alert",
            "vbucks timed": "V-Bucks Alert",
            "v-bucks timed": "V-Bucks Alert",
            "vbucks alert": "V-Bucks Alert",
            "v-bucks alert": "V-Bucks Alert",
            "legendary": "Legendary Alert",
            "legendary timed": "Legendary Alert",
            "legendary alert": "Legendary Alert",
            "mythic": "Mythic Alert",
            "mythic timed": "Mythic Alert",
            "mythic alert": "Mythic Alert",
            "daily llama": "Daily Llama",
            "daily reset": "Daily Llama",
            "free games": "Free Games",
            "free game": "Free Games",
            "giveaways": "Giveaways",
            "giveaway": "Giveaways",
            "contests": "Contests",
            "contest": "Contests",
            "ssd helper": "SSD Specialist",
            "ssd specialist": "SSD Specialist",
            "ssd": "SSD Specialist",
            "llama specialist": "Llama Specialist",
            "llama helper": "Llama Specialist",
            "llama questions": "Llama Specialist",
            "missions helper": "Missions Saviour",
            "missions saviour": "Missions Saviour",
            "missions savior": "Missions Saviour",
            "stw helper": "Missions Saviour",
            "lfg helper": "Missions Saviour",
            "questions helper": "Questions Helper",
            "questions help": "Questions Helper",
            "questions": "Questions Helper",
            "chkoupinabot fan/tester": "Chkoupinabot Fan/Tester",
            "chkoupinabot": "Chkoupinabot Fan/Tester",
            "chkoupinabot fan": "Chkoupinabot Fan/Tester",
            "chkoupinabot tester": "Chkoupinabot Fan/Tester",
            "chkoupinafan": "Chkoupinabot Fan/Tester",
            "cuddly": "Cuddly"
        }
        self.shown_aliases = {
            "Save the World": ["stw", "pve"],
            "BattleRoyale": ["br", "pvp"],
            "Server News": ["news"],
            "StW News": ["pve news"],
            "BR News": ["pvp news"],
            "V-Bucks Alert": ["v-bucks", "v-bucks timed", "vbucks"],
            "Legendary Alert": ["legendary", "legendary timed"],
            "Mythic Alert": ["mythic", "mythic timed"],
            "Daily Llama": ["daily reset"],
            "Free Games": ["free game"],
            "Giveaways": ["giveaway"],
            "Contests": ["contest"],
            "SSD Specialist": ["ssd", "ssd helper"],
            "Llama Specialist": ["llama questions", "llama helper"],
            "Missions Saviour": ["lfg helper", "stw helper"],
            "Questions Helper": ["questions"],
            "Chkoupinabot Fan/Tester": ["chkoupinabot fan", "chkoupinafan", "chkoupinabot tester"]
        }

        # TODO Rework all of the above to be stored in a json file instead of being hardcoded

    @commands.command(name="roles", aliases=["selfroles"])
    async def roles(self, ctx):
        embed = discord.Embed(title=self.lang("cmds.roles.embed_title"),
                              description=self.lang("cmds.roles.embed_desc").format(prefix=self.prefix))
        for category in self.self_roles.keys():
            message = " " + self.lang(f"cmds.roles.categories.{category}.description") + ".\n"
            for key in self.self_roles[category].keys():
                line = self.lang("cmds.roles.role_format").format(
                    role=f"<@&{self.self_roles[category][key]}>", aliases="`, `".join(self.shown_aliases[key]),
                    description=self.lang(f"cmds.roles.roles_description.{key}"))
                message = f"{message}{line}"
            embed.add_field(name=self.lang(f"cmds.roles.categories.{category}.name")+":",
                            value=message)
        await ctx.send(embed=embed)

    @commands.command(name="getrole", aliases=["addrole", "selfrole", "getr", "giverole"])
    async def getrole(self, ctx, *, role: str=None):
        if role is None:
            return await ctx.invoke(self.roles)
        if discord.utils.get(ctx.author.roles, id=327185317167759360) is None:
            return await ctx.send(self.lang("cmds.getrole.missing_member_role"))
        role = role.lower().replace("role", "").strip(" ")
        if role in ["owner", "admin"]:
            return await ctx.send(self.lang(f"cmds.getrole.{role}"))
        if role not in self.self_role_aliases.keys():
            return await ctx.send(self.lang("cmds.getrole.incorrect_role").format(prefix=self.prefix))
        role = self.self_role_aliases[role]
        if isinstance(self.self_roles_ids_dict[role], dict):
            if discord.utils.get(ctx.author.roles,
                                 id=self.self_roles_ids_dict[self.self_roles_ids_dict[role]["required"]]):
                if not discord.utils.get(ctx.author.roles, id=self.self_roles_ids_dict[role]["id"]):
                    await ctx.author.add_roles(discord.Object(self.self_roles_ids_dict[role]["id"]),
                                               reason="User used getrole to get a role")
                    await ctx.send(self.lang("cmds.getrole.success").format(role=role))
                else:
                    await ctx.send(self.lang("cmds.getrole.already_equipped").format(prefix=self.prefix))
            else:
                await ctx.send(self.lang("cmds.getrole.required_not_equipped").format(
                    required=self.self_roles_ids_dict[role]["required"]))
        else:
            if not discord.utils.get(ctx.author.roles, id=self.self_roles_ids_dict[role]):
                await ctx.author.add_roles(discord.Object(self.self_roles_ids_dict[role]),
                                           reason="User used getrole to get a role")
                await ctx.send(self.lang("cmds.getrole.success").format(role=role))
            else:
                await ctx.send(self.lang("cmds.getrole.already_equipped").format(prefix=self.prefix))

    @commands.command(name="delrole", aliases=["removerole", "unequiprole", "deleterole", "takerole"])
    async def delrole(self, ctx, *, role: str=None):
        if role is None:
            return await ctx.send(self.lang("cmds.delrole.no_args").format(prefix=self.prefix))
        role = role.lower().replace("role", "").strip(" ")
        if role not in self.self_role_aliases.keys():
            return await ctx.send(self.lang("cmds.getrole.incorrect_role").format(prefix=self.prefix))
        role = self.self_role_aliases[role]
        role_id = self.self_roles_ids_dict[role]["id"] if isinstance(self.self_roles_ids_dict[role], dict) else \
            self.self_roles_ids_dict[role]
        if discord.utils.get(ctx.author.roles, id=role_id):
            await ctx.author.remove_roles(discord.Object(role_id), reason="User requested role removal")
            await ctx.send(self.lang("cmds.delrole.success").format(role=role))
        else:
            await ctx.send(self.lang("cmds.delrole.role_not_equipped").format(prefix=self.prefix, role=role))

    @commands.command(name="member")
    async def member(self, ctx):
        if discord.utils.get(ctx.author.roles, id=327185317167759360):
            m = await ctx.send(self.lang("cmds.member.already_member").format(userid=ctx.author.id))
            await ctx.message.delete()
            await asyncio.sleep(5)
            return await m.delete()
        await ctx.author.add_roles(discord.Object(id=self.member_role_id), reason="Used the member command")
        await ctx.send(self.lang("cmds.member.success").format(username=ctx.author.display_name,
                                                               roleid=self.member_role_id))
        await ctx.message.delete()


def setup(bot):
    bot.add_cog(RolesCog(bot))
