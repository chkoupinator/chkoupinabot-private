import discord
import traceback
import sys
from discord.ext import commands
from utils.exceptions import MessageTooLong, WrongChannel
from lang.lang_wrap import LangWrap
from chkoupinabot import ChkoupinaBot

lang_settings = {
    "cmds": {
        "help": {
            "help": {
                "name": "help",
                "usage": "`{prefix}help [command | module]`\nshows the help for a command or a module if specified, "
                         "shows a list of all the modules and their commands else",
                "short_desc": "the command that shows this",
                "long_desc": "help is a useful command that can be used either to show all the modules and commands of "
                             "the bot, or to get help for a specific command and its usage"
            },
            "not_found": "module/command not found..."
        },
        "credits": {
            "help": {
                "name": "credits",
                "usage": "`{prefix}credits` shows the credits for the bot",
                "short_desc": "shows the credits of the bot",
                "long_desc": "a command that shows the credits for the creation of the bot as well as how you can"
                             "help on its development"
            },
            "title": "credits for Chkoupinabot",
            "desc": "Chkoupinabot is a bot made by Chkoupinator#7777, this version is made to work on /r/Fortnite's"
                    " Discord solely.\n"
                    "If you want to donate to Chkoupinator to keep the bot's server running or upgrade it or for "
                    "any other reason, please contact him in a DM (direct message)."
        }
    },
    "formatter": {
        "cogs_list_title": "help for chkoupinabot",
        "cogs_list_desc": "you will find below a list of the modules of the bot and the commands that they contain "
                          "with a small description",
        "cog_commands_list": "{short_desc}\n**__`Commands:`__**\n",
        "help_for_command": "help for the command",
        "command_aliases": "aliases:",
        "command_usage": "usage:",
        "help_for_cog": "help for the module",
        "cog_commands": "commands of the module:"
    },
    "error_handler": {
        "not_recognized": "command not recognized",
        "did_you_mean": "did you mean",
        "not_owner": "you're not my owner! I won't listen to you, go away!",
        "legacy_command": "you are trying to use a legacy (blargbot) command, please use `{prefix}getrole`, "
                          "`{prefix}platform and `{prefix}region` instead or do `{prefix}help` for more info"
    },
    "command_in_wrong_channel": "the purpose of this channel is not for bot commands, please go to "
                                "<#463162195342131230> or <#275320521338519552> for that",
    "help": {
        "name": "help module",
        "short_desc": "lists commands and takes care of general errors",
        "long_desc": "takes care of listing commands and handles some of the errors"
    }
}


class HelpCog(LangWrap):
    def __init__(self, bot: ChkoupinaBot):
        super().__init__(bot)
        bot.help = self
        self.aliases_dict = dict()
        self.cog_names = dict()
        self.cmd_names = dict()
        self.allowed_cmd_channels = [275320521338519552,  # bot-commands
                                     334956664166023168,  # tavern-basement
                                     463162195342131230,  # get-your-roles
                                     332300593500651522,  # mod-botspam
                                     449480741680709652   # chkoupinabot-testing
                                     ]
        self.channel_bypass_roles = [332221421247922177,  # Server Staff
                                     448247918521090058,  # Helper
                                     332229285420990466]  # Admin

        def check_channel(ctx: commands.Context):
            if ctx.channel.id in self.allowed_cmd_channels or isinstance(ctx.channel, discord.DMChannel):
                return True
            for role in ctx.author.roles:
                if role.id in self.channel_bypass_roles:
                    return True
            raise WrongChannel
        self.bot.add_check(check_channel)

    def load_dicts(self):
        for cmd in self.bot.commands:
            if cmd.cog_name:
                self.aliases_dict[f"cogs.{cmd.cog_name}.cmds.{cmd.name}"] = cmd.aliases
        for lang in self.bot.lang.langs_dict.keys():
            for cog in self.bot.lang.langs_dict[lang]["cogs"].keys():
                self.cog_names[self.get_name(f"cogs.{cog}").lower()] = f"cogs.{cog}"
                self.cog_names[cog.lower()] = f"cogs.{cog}"
        for lang in self.bot.lang.langs_dict.keys():
            for cog in self.bot.lang.langs_dict[lang]["cogs"].keys():
                for cmd in self.bot.lang.langs_dict[lang]["cogs"][cog]["cmds"].keys():
                    self.cmd_names[self.get_name(f"cogs.{cog}.cmds.{cmd}").lower()] = f"cogs.{cog}.cmds.{cmd}"
                    self.cmd_names[cmd.lower()] = f"cogs.{cog}.cmds.{cmd}"

                    for alias in self.aliases_dict[f"cogs.{cog}.cmds.{cmd}"]:
                        self.cmd_names[alias.lower()] = f"cogs.{cog}.cmds.{cmd}"

    def get_name(self, path, *, language=None):
        return self.bot.lang.get_string(path+".help.name", language=language)

    def get_short_desc(self, path, *, language=None):
        return self.bot.lang.get_string(path+".help.short_desc", language=language)

    def get_long_desc(self, path, *, language=None):
        return self.bot.lang.get_string(path+".help.long_desc", language=language)

    def get_usage(self, path, *, language=None):
        return self.bot.lang.get_string(path+".help.usage", language=language).format(prefix=self.prefix)

    def help_formatter(self, what, *, language=None):
        if what is None:
            embed = discord.Embed(title=self.lang("formatter.cogs_list_title"),
                                  description=self.lang("formatter.cogs_list_desc"))
            for cog in self.bot.lang.langs_dict["None"]["cogs"].keys():
                message = self.lang("formatter.cog_commands_list").format(short_desc=self.get_short_desc('cogs.'+cog),
                                                                          language=language)
                longest_cmd = 0
                for cmd in self.bot.lang.langs_dict["None"]["cogs"][cog]["cmds"].keys():
                    if len(cmd) > longest_cmd:
                        longest_cmd = len(cmd)
                longest_cmd += 3
                for cmd in self.bot.lang.langs_dict["None"]["cogs"][cog]["cmds"].keys():
                    message += "{0:12}:` {1}\n".format(f"-`{self.prefix}{str(cmd)}",
                        self.get_short_desc(f"cogs.{cog}.cmds.{str(cmd)}", language=language))
                embed.add_field(name=self.get_name("cogs."+cog),
                                value=message)
            return embed
        else:
            if what.lower() in self.cog_names.keys():
                what = self.cog_names[what.lower()]
            elif what.lower() in self.cmd_names.keys():
                what = self.cmd_names[what.lower()]
            else:
                return None

            if "cmds" in what:
                embed = discord.Embed(title=f"{self.lang('formatter.help_for_command')} "
                                            f"\"{self.get_name(what, language=language)}\":",
                                      description=self.get_long_desc(what, language=language))
                if self.aliases_dict[what]:
                    embed.add_field(name=self.lang("formatter.command_aliases"),
                                    value=", ".join(f"`{a}`" for a in self.aliases_dict[what]))
                embed.add_field(name=self.lang("formatter.command_usage").format(prefix=self.prefix),
                                value=self.get_usage(what), inline=False)
            else:
                embed = discord.Embed(title=f"{self.lang('formatter.help_for_cog')} "
                                            f"\"{self.get_name(what, language=language)}\":",
                                      description=self.get_long_desc(what, language=language))
                longest_cmd = 0
                cog = self.bot.lang.langs_dict["None"]["cogs"][what.split(".")[-1]]
                for cmd in cog["cmds"].keys():
                    if len(cmd) > longest_cmd:
                        longest_cmd = len(cmd)
                longest_cmd += 3
                message = ""
                for cmd in cog["cmds"].keys():
                    message += "-`{0:{2}}:` {1}\n".format(f"{self.prefix}{str(cmd)}", self.get_short_desc(
                        f"cogs.{what.split('.')[-1]}.cmds.{str(cmd)}", language=language), longest_cmd)

                embed.add_field(name=self.lang("formatter.cog_commands"), value=message)
            return embed

    @commands.command(name="help", aliases=["cmds", "commands"])
    async def help(self, ctx, *, what: str=None):
        embed = self.help_formatter(what=what)
        if embed:
            if what is None:
                await ctx.author.send(embed=embed)
            else:
                await ctx.send(embed=embed)
        else:
            await ctx.send(self.lang("cmds.help.not_found"))

    @commands.command(name="credits", aliases=["acknowledgements", "dev", "developer"])
    async def credits(self, ctx):
        await ctx.send(
            embed=discord.Embed(title=self.lang('cmds.credits.title'), description=self.lang('cmds.credits.desc')))

    async def on_command_error(self, ctx, error):
        if isinstance(error, WrongChannel):
            return await ctx.send(self.lang("command_in_wrong_channel"))
        if isinstance(error, commands.NotOwner):
            return await ctx.send(self.lang("error_handler.not_owner"))

        if hasattr(ctx.command, 'on_error'):
            return
        if isinstance(error, commands.CommandInvokeError) and isinstance(error.original, MessageTooLong):
            msg = error.original.message
            print("A message was too long but I caught it and split it, this was the message:")
            print(msg)

            def splitter(m, limit, char):
                l = list()
                o = 0
                if char:
                    banana = m.split(char)
                else:
                    banana = list(m)

                for i in banana:
                    if len(i) > limit:
                        return 0
                i = 1
                while i < len(banana):
                    if len(char.join(banana[o:i])) > limit:
                        l.append(char.join(banana[o:i - 1]))
                        o = i-1
                    else:
                        i += 1
                l.append(char.join(banana[o:i - 1]))
                return l

            wrap_begin = str()
            wrap_end = str()
            if "```" in msg:
                wrap_begin = msg.split("\n")[0]+"\n"
                msg = "\n".join(msg.split("\n")[1:])
                wrap_end = "```"

            while True:
                split = splitter(msg, 1999-len(wrap_begin)-len(wrap_end), "\n")
                if isinstance(split, int):
                    split = splitter(msg, 1999 - len(wrap_begin) - len(wrap_end), " ")
                    if isinstance(split, int):
                        split = splitter(msg, 1999 - len(wrap_begin) - len(wrap_end), "")
                        break
                    else:
                        break
                else:
                    break
            for i in split:
                await ctx.send(f"{wrap_begin}{i}{wrap_end}")
            return
        elif isinstance(error, commands.CommandNotFound):
            legacy_commands = ["daily_llama", "epic_timed", "eu", "ps4", "xb1", "legendary_timed", "mobile", "na",
                               "oce", "pc", "phone"]
            if ctx.invoked_with.lower() in legacy_commands:
                await ctx.send(self.lang("error_handler.legacy_command").format(prefix=self.prefix))

        #     possible_commands = [self.lang("error_handler.not_recognized")]
        #     command_text = ctx.invoked_with
        #     commands_list = list()
        #     for command in self.bot.commands:
        #         commands_list.append(command.name)
        #         commands_list.extend(command.aliases)
        #     for key in commands_list:
        #         if (command_text in key and len(key.replace(command_text, "")) < 2) or \
        #                 (key in command_text and len(command_text.replace(key, "")) < 4):
        #             if len(possible_commands) == 1:
        #                 possible_commands.append(self.lang("error_handler.did_you_mean")
        #                                          + " **`{}{}`**".format(self.prefix, key))
        #             else:
        #                 possible_commands.append("**`{}{}`**".format(self.prefix, key))
        #     if len(possible_commands) > 2:
        #         await ctx.send(
        #             " or ".join([", ".join(possible_commands[0:-1]), possible_commands[-1]]) + "?")
        #     elif len(possible_commands) == 2:
        #         await ctx.send(", ".join(possible_commands) + "?")
        #     return

        print('Ignoring exception in command {}:'.format(ctx.command), file=sys.stderr)
        traceback.print_exception(type(error), error, error.__traceback__, file=sys.stderr)


def setup(bot):
    bot.add_cog(HelpCog(bot))
