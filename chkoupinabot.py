from discord.ext import commands
import discord
from utils.prefs_io import Preferences


class ChkoupinaBot(commands.Bot):
    def __init__(self, *, description, command_prefix, case_insensitive, prefs: Preferences):
        super().__init__(description=description, command_prefix=command_prefix, case_insensitive=case_insensitive)
        self.help = None
        self.achievements = None
        self.prefs = prefs  # type: Preferences
        self.lang = None
        self.chkoupinator = None  # type: discord.User

    async def get_prefix(self, message):
        if not message.guild:
            return commands.when_mentioned_or(self.prefs.prefix, '')(self, message)

        return await super().get_prefix(message)
