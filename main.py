import discord
import sys
import traceback
from utils import prefs_io
from lang.lang import Lang
from discord.ext import commands
from utils.prefs_io import Preferences


class ChkoupinaBot(commands.Bot):
    def __init__(self, *, description, command_prefix, case_insensitive, prefs: Preferences):
        super().__init__(description=description, command_prefix=command_prefix, case_insensitive=case_insensitive)
        self.help = None
        self.achievements = None
        self.prefs = prefs
        self.lang = Lang(self)
        self.chkoupinator = None  # type: discord.User

    async def get_prefix(self, message):
        if not message.guild:
            return commands.when_mentioned_or(self.prefs.prefix, '')(self, message)

        return await super().get_prefix(message)


prefs = prefs_io.Preferences()


chkoupinabot = ChkoupinaBot(description=prefs.description, command_prefix=commands.when_mentioned_or(prefs.prefix),
                            case_insensitive=True, prefs=prefs)

init_cogs = ['utility', 'profiles', "social", "utility", "karma", "achievements", "roles", "notifications", "economy",
             "moderation"]


if __name__ == '__main__':
    for cog in init_cogs:
        try:
            chkoupinabot.load_extension("cogs."+cog)
        except discord.ClientException or ImportError:
            print(f"Error: Failed to load extension \"{cog}\"", file=sys.stderr)
            traceback.print_exc()
    try:
        chkoupinabot.remove_command("help")
        chkoupinabot.load_extension("cogs.help")
    except discord.ClientException or ImportError:
        print(f"Error: Failed to load the help extension", file=sys.stderr)
        traceback.print_exc()
    else:
        chkoupinabot.help.load_dicts()


@chkoupinabot.event
async def on_ready():
    print('Logged in')
    print(f'Name: {chkoupinabot.user.name}')
    print('ID: ' + str(chkoupinabot.user.id))
    print('Prefix: '+chkoupinabot.prefs.prefix)
    print(f'Discord version: {discord.__version__}')


chkoupinabot.run(chkoupinabot.prefs.hkey)
