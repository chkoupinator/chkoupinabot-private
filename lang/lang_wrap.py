from chkoupinabot import ChkoupinaBot


class LangWrap:
    def __init__(self, bot: ChkoupinaBot):
        self.bot = bot

    def lang(self, path, *, language=None) -> str:
        # print("cogs."+self.__class__.__name__+"." + path)
        return self.bot.lang.get_string("cogs."+self.__class__.__name__+"." + path, language=language)

    @property
    def prefix(self):
        return self.bot.prefs.prefix
