import utils
import cogs
import json
import os
import sys
import copy
import traceback


empty_lang_settings = {
    "cmds": {
        "some_command": {
            "help": {
                "name": "",
                "usage": "{prefix}some_command ...",
                "short_desc": "",
                "long_desc": ""
            }
        }
    },
    "help": {
        "name": "",
        "short_desc": "",
        "long_desc": ""
    }
}


class PartiallyUndefined(Exception):
    pass


def deep_search(d, p):
    if '.' not in p:
        return d[p]
    else:
        return deep_search(d[p.split('.')[0]], '.'.join(p.split('.')[1:]))


def set_empty(d, p, t):
    if len(p.split('.')) == 1:
        d[p] = t()
    else:
        try:
            set_empty(d[p.split('.')[0]], '.'.join(p.split('.')[1:]), t)
        except KeyError:
            d[p.split('.')[0]] = empty_dict_path('.'.join(p.split('.')[1:]), t)


def empty_dict_path(p, t):
    if len(p.split('.')) == 1:
        return {p: t()}
    else:
        return {p.split('.')[0]: empty_dict_path('.'.join(p.split('.')[1:]), t)}


def langs_dict_loader(lang):
    try:
        with open(os.path.join("lang", lang + ".json"), 'r') as lang_file:
            return json.load(lang_file)
    except FileNotFoundError:
        print("File lang", lang, "json not found! Creating one from an empty default one", sep=".", file=sys.stderr)
        with open(os.path.join("lang", lang + ".json"), 'w+') as lang_file:
            json.dump(Lang.get_empty_default(), lang_file, indent=4)
        with open(os.path.join("lang", lang + ".json"), 'r') as lang_file:
            return json.load(lang_file)


class Lang:
    default_tree = {
        "cogs": cogs.lang_map,
        "utils": utils.lang_map,
        "main": {}
    }

    def __init__(self, bot):
        self.bot = bot
        self.langs_dict = None
        self.reload_lang()

        # Deleted part, reload_lang should do exactly the same job as what's commented out here

        # self.langs_dict = dict()
        # for lang in self.bot.prefs.supported_languages:
        #     self.langs_dict[lang] = langs_dict_loader(lang)
        # default = Lang.default_tree
        #
        # def deep_navigation(model, d, path):
        #     nonlocal warnings
        #     for key in list(model.keys()):
        #         if key in list(d.keys()):
        #             if isinstance(d[key], dict):
        #                 deep_navigation(model[key], d[key], path+"."+key)
        #             elif isinstance(d[key], str) and len(d[key]) == 0:
        #                 warning = f"Warning: Empty string at `{path}.{key}`!"
        #                 if warning not in warnings:
        #                     print(warning, file=sys.stderr)
        #                 warnings = warnings + warning
        #         else:
        #             if isinstance(model[key], dict):
        #                 warning = f"Warning: Missing dict at `{path}.{key}`! Adding empty dict"
        #                 if warning not in warnings:
        #                     print(warning, file=sys.stderr)
        #                 warnings = warnings + warning
        #                 d[key] = dict()
        #             elif isinstance(model[key], str):
        #                 warning = f"Warning: Missing string at `{path}.{key}`! Adding empty string"
        #                 if warning not in warnings:
        #                     print(warning, file=sys.stderr)
        #                 warnings = warnings + warning
        #                 d[key] = str()
        #             elif isinstance(model[key], list):
        #                 warning = f"Warning: Missing list at `{path}.{key}`! Adding empty list"
        #                 if warning not in warnings:
        #                     print(warning, file=sys.stderr)
        #                 warnings = warnings + warning
        #                 d[key] = list()
        #             raise PartiallyUndefined
        #
        # self.langs_dict["None"] = default
        # self.save_lang("None")
        #
        # warnings = ""
        # for lang in list(self.langs_dict.keys()):
        #     while True:
        #         try:
        #             deep_navigation(default, self.langs_dict[lang], "lang."+lang)
        #         except PartiallyUndefined:
        #             pass
        #         else:
        #             self.save_lang(lang)
        #             break

    @staticmethod
    def get_empty_default():
        def empty_values(d):
            for key in d.keys():
                if isinstance(d[key], dict):
                    empty_values(d[key])
                elif isinstance(d[key], str):
                    d[key] = str()
                elif isinstance(d[key], list):
                    d[key] = list()
                else:
                    print(f"Error {d[key]} is neither a dict nor an str nor a list!", file=sys.stderr)
            return d

        return empty_values(copy.deepcopy(Lang.default_tree))

    def save_lang(self, language):
        with open(os.path.join("lang", language + ".json"), 'w+') as lang_file:
            json.dump(self.langs_dict[language], lang_file, indent=4)
            lang_file.truncate()

    def get_string(self, path, *, language=None,  defaults_to="None"):
        if language is None:
            language = self.bot.prefs.language
        try:
            result = deep_search(self.langs_dict[language], path)
            if result == "":
                if language != defaults_to:
                    print(f'Error: The string for {path} in the "{language}" language is empty! '
                          f'Falling back to the "{"default" if defaults_to == "None" else defaults_to}" '
                          f'language files...', file=sys.stderr)
                    return self.get_string(path, language=defaults_to)
                else:
                    print(f'Error: The default string for {path} is empty!', file=sys.stderr)

            elif result == []:
                if language != defaults_to:
                    print(f'Error: The list for {path} in the "{language}" language is empty! '
                          f'Falling back to the "{"default" if defaults_to == "None" else defaults_to}" '
                          f'language files...', file=sys.stderr)
                    return self.get_string(path, language=defaults_to)
                else:
                    print(f'Error: The default list for {path} is empty!', file=sys.stderr)

            else:
                return result

        except KeyError:
            # traceback.print_exc()
            if language != defaults_to:
                print(f'Error: Failed to find the string for {path} for the "{language}" language... resorting to '
                      f'default string', file=sys.stderr)
                default = self.get_string(path, language=defaults_to)
                if default is not None:
                    set_empty(self.langs_dict[language], path, type(default))
                    self.save_lang(language)
                    return default
                else:
                    print(f'Error: The default string for {path} is a NoneType!', file=sys.stderr)
            else:
                traceback.print_exc()
                print(f'Error: The specified path "{path}" does not exist in the default lang files!', file=sys.stderr)

    def reload_lang(self):
        self.langs_dict = dict()
        for lang in self.bot.prefs.supported_languages:
            self.langs_dict[lang] = langs_dict_loader(lang)
        default = Lang.default_tree

        def deep_navigation(model, d, path):
            nonlocal warnings
            for key in list(model.keys()):
                if key in list(d.keys()):
                    if isinstance(d[key], dict):
                        deep_navigation(model[key], d[key], path+"."+key)
                    elif isinstance(d[key], str) and len(d[key]) == 0:
                        warning = f"Warning: Empty string at `{path}.{key}`!"
                        if warning not in warnings:
                            print(warning, file=sys.stderr)
                        warnings = f"{warnings}\n-{warning}"
                else:
                    if isinstance(model[key], dict):
                        warning = f"Warning: Missing dict at `{path}.{key}`! Adding empty dict"
                        if warning not in warnings:
                            print(warning, file=sys.stderr)
                        warnings = f"{warnings}\n-{warning}"
                        d[key] = dict()
                    elif isinstance(model[key], str):
                        warning = f"Warning: Missing string at `{path}.{key}`! Adding empty string"
                        if warning not in warnings:
                            print(warning, file=sys.stderr)
                        warnings = f"{warnings}\n-{warning}"
                        d[key] = str()
                    elif isinstance(model[key], list):
                        warning = f"Warning: Missing list at `{path}.{key}`! Adding empty list"
                        if warning not in warnings:
                            print(warning, file=sys.stderr)
                        warnings = f"{warnings}\n-{warning}"
                        d[key] = list()
                    raise PartiallyUndefined

        warnings = ""
        for lang in list(self.langs_dict.keys()):
            while True:
                try:
                    deep_navigation(default, self.langs_dict[lang], "lang."+lang)
                except PartiallyUndefined:
                    pass
                else:
                    self.save_lang(lang)
                    break

        self.langs_dict["None"] = default
        self.save_lang("None")
        try:
            return warnings
        except NameError:
            return
