import json
import collections


Prefs = collections.namedtuple("Prefs", "prefix, title, description, language, supported_languages, HKEY, "
                                        "imgurClientID, respond, exp_cooldown, main_guild, currency,"
                                        " current_season, previous_season")


def save_prefs(prefs):
    with open("PREFS.json", 'w+') as PREFS_file:
        json.dump(dict(prefs._asdict()), PREFS_file, indent=4)
        PREFS_file.truncate()


def load_prefs():
    with open("PREFS.json", "r") as PREFS_file:
        return Prefs(**json.load(PREFS_file))


class Preferences:
    def __init__(self, *, prefs: Prefs=None):
        self.saved = load_prefs()
        if prefs is None:
            self.current = self.saved
        else:
            self.current = prefs

    def reload(self):
        self.saved = load_prefs()
        self.current = self.saved

    def save(self):
        self.saved = self.current
        save_prefs(self.saved)

    def add_prefix(self, prefix):
        self.current.prefix.append(prefix)

    @property
    def prefix(self):
        return self.current.prefix[0]

    @property
    def description(self):
        return self.current.description

    @property
    def hkey(self):
        return self.current.HKEY

    @property
    def supported_languages(self):
        return self.current.supported_languages

    @property
    def language(self):
        return self.current.language

    @property
    def exp_cooldown(self):
        return self.current.exp_cooldown

    @property
    def main_guild(self):
        return self.current.main_guild

    @property
    def currency(self):
        return self.current.currency

    @property
    def current_season(self):
        return self.current.current_season

    @property
    def previous_season(self):
        return self.current.previous_season


# PREFS Reinitialization:
def reset_prefs():
    save_prefs(Prefs(prefix=["$"], title="Chkoupinabot, the highly unstable but functioning bot",
                     description="A bot made by **Chkoupinator#1064** for personal usage and needs. "
                                 "Has a profile system and some fun commands and lotsa features to come!",
                     language="en", supported_languages=["en"],
                     HKEY="NDQzNjE2MDExNzQxMjk4Njk4.DdU9ag.82WHp95CuPI-tfK76ggXzQUfg0s",
                     imgurClientID="9238df1d96a931e",
                     respond={"henlo": "henlo buitiful"}, exp_cooldown=5, main_guild=326542974601134083,
                     currency="cbucks", current_season="Jan-Mar 2019", previous_season="Oct-Dec 2018"))
