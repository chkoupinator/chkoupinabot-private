import subprocess
import traceback
from platform import system


def icmp_ping(hostname):
    try:
        output = subprocess.check_output(["ping", "-n" if system().lower() == 'windows' else "-c", "1", hostname])
        output = str(output)
        for x in list(output.split(" ")):
            if "time=" in x:
                return float(x.strip("time=").strip("ms"))
    except ValueError or subprocess.CalledProcessError:
        print(f"Ping to {hostname} failed")
        traceback.print_exc()
        return None


def user_to_detailed_str(user):
    return f"{user} ({user.id})"
