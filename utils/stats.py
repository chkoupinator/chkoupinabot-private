import json


def load():
    with open("stats.json") as stats_file:
        return json.load(stats_file)


def save(stats_dict):
    with open("stats.json", "w+") as stats_file:
        json.dump(stats_dict, stats_file, indent=2)
        stats_file.truncate()
        return True

