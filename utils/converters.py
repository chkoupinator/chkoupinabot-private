from discord.ext.commands import Converter, BadArgument
from discord import utils


class MemberConverter(Converter):
    async def convert(self, ctx, argument: str):
        try:
            member = utils.get(ctx.guild.members, id=int(argument.lstrip("<@").lstrip("!").rstrip(">")))
            if member:
                return member
        except ValueError:
            pass

        if len(argument) > 5 and argument[-5] == '#':
            member = utils.get(ctx.guild.members, name=argument[:-5], discriminator=argument[-4:])
            if member:
                return member

        def predicate(m):
            return (m.nick and m.nick.lower() == argument.lower()) or m.name.lower() == argument.lower()
        member = utils.find(predicate, ctx.guild.members)
        if member:
            return member

        raise BadArgument('Member "{}" not found'.format(argument))
