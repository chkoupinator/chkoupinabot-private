from discord.ext.commands import CommandError


class MessageTooLong(Exception):
    def __init__(self, msg):
        self.message = msg


class AchievementNotFound(Exception):
    def __init__(self, name):
        self.name = name


class WrongChannel(CommandError):
    pass
