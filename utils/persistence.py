import json


def load():
    with open("persistence.json") as persistence_file:
        return json.load(persistence_file)


def get(what):
    return load()[what]


def save(where, what):
    new = load()
    new[where] = what
    with open("persistence.json", "w+") as persistence_file:
        json.dump(new, persistence_file, indent=2)
        persistence_file.truncate()
        return True


