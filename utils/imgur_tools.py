import requests
import sys


def upload_from_url(img_url, client_id):
    url = "https://api.imgur.com/3/image"
    payload = {"image": img_url,
               "type": "url"}
    headers = {'Authorization': 'Client-ID ' + client_id}
    answer = requests.request("POST", url, data=payload, headers=headers).json()
    if answer["success"]:
        print("Imgur upload success, response was: " + str(answer))
        return answer['data']['link']
    else:
        print("Imgur upload failed, response was:" + str(answer), file=sys.stderr)
        return


# import pprint
# pprint.pprint(upload_from_url("https://cdn.discordapp.com/avatars/216468217495945216/
# a_5ea98148fd5e045084b0cff710ad7077.gif"))
